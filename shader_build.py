import json
import os, errno
import sys
import subprocess

def printHelp():
    print("missing source files")

class ShaderSource:
    def __init__(self, path):
        self.source = path
        self.src_entry_point = "main"
        self.dst_entry_point = "main"

    def __str__(self):
        return "ShaderSource: \"" + self.source + "\" src_entry_point:\"" + self.src_entry_point + "\" dst_entry_point:\"" + self.dst_entry_point + "\""

    def setSrcEntryPoint(self, entry_point):
        self.src_entry_point = entry_point

    def setDstEntryPoint(self, entry_point):
        self.dst_entry_point = entry_point

    def build(self, build_dir):
        output_path = os.path.join(build_dir, self.source + ".spv")

        try:
            os.makedirs(os.path.dirname(output_path))
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

        if os.path.exists(output_path):
            os.remove(output_path)

        cmd = ("glslangValidator" + " -V" +
            " -e " + self.dst_entry_point + 
            " --source-entrypoint " + self.src_entry_point + 
            " -o " + output_path +
            " " + self.source)

        print("[" + cmd + "]")
        subprocess.run(cmd)

        return output_path

class ShaderModule:
    def __init__(self, name, out_dir):
        self.name = name
        self.out_dir = out_dir
        self.output = os.path.join(out_dir, name + ".spv")
        self.sources = []

    def __str__(self):
        ret = "ShaderModule: \"" + self.name + "\" out:\"" + self.output + "\"\n"
        for source in self.sources:
            ret = ret + "    " + str(source) + "\n"
        return ret

    def setOutFilename(self, out):
        self.output = os.path.join(self.out_dir, out)

    def addSource(self, source):
        self.sources.append(source)

    def build(self, build_dir):
        try:
            os.makedirs(os.path.dirname(self.output))
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

        if os.path.exists(self.output):
            os.remove(self.output)

        src_list = ""
        for src in self.sources:
            src_list = src_list + src.build(build_dir) + " "
        
        cmd = ("spirv-link" +
            " -o " + self.output +
            " " + src_list)

        print("[" + cmd + "]")
        subprocess.run(cmd)

"""
cwd: The relative working directory of the modules file
out: The output directory
info: The modules info datastructure
module: A array where all modules will be placed in
"""
def parseModulesInfo(cwd, out, info, modules):
    out_dir = os.path.join(out, cwd)

    for name, mod_data in info.items():
        module = ShaderModule(name, out_dir)

        if "out_name" in mod_data:
            module.setOutFilename(mod_data["out_name"])

        for src in mod_data["sources"]:
            source = ShaderSource(os.path.join(cwd, src["source"]))
            if "entry_point" in src:
                source.setDstEntryPoint(src["entry_point"])
            module.addSource(source)

        modules.append(module)


def main():
    print("Shader build tool")
    if len(sys.argv) == 0:
        printHelp()
        sys.exit()

    modules = []
    with open("shaders/modules.json", "r") as file:
        parseModulesInfo("shaders", "build", json.load(file), modules)

    modules[0].build("build/shader_build")

    print(modules[0])
    

if __name__ == "__main__":
    main()