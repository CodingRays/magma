#include "glfw_window.hpp"

namespace magma::window_system {

    GLFWWindow::ImageData::ImageData(VkDevice device) : device{ device } {
    }

    GLFWWindow::ImageData::ImageData(ImageData&& other) : 
        image{ other.image },
        view{ other.view },
        command_buffer{ other.command_buffer },
        descriptor{ other.descriptor },
        framebuffer{ other.framebuffer },
        ready_fence{ other.ready_fence },
        device{ other.device } {
    
        other._clear();
    }

    GLFWWindow::ImageData::~ImageData() {
        _delete();
    }

    GLFWWindow::ImageData& GLFWWindow::ImageData::operator =(ImageData&& other) {
        _delete();
        device = other.device;

        if(device != VK_NULL_HANDLE) {
            image = other.image;
            view = other.view;
            command_buffer = other.command_buffer;
            descriptor = other.descriptor;
            framebuffer = other.framebuffer;
            ready_fence = other.ready_fence;

            other._clear();
        }

        return *this;
    }

    void GLFWWindow::ImageData::_delete() {
        if(device != VK_NULL_HANDLE) {
            if(ready_fence) {
                vkDestroyFence(device, ready_fence, nullptr);
                ready_fence = VK_NULL_HANDLE;
            }

            if(framebuffer) {
                vkDestroyFramebuffer(device, framebuffer, nullptr);
                framebuffer = VK_NULL_HANDLE;
            }

            // Both are allocated from a pool
            descriptor = VK_NULL_HANDLE;
            command_buffer = VK_NULL_HANDLE;

            if(view) {
                vkDestroyImageView(device, view, nullptr);
                view = VK_NULL_HANDLE;
            }

            // Managed by the swapchain
            image = VK_NULL_HANDLE;

            device = VK_NULL_HANDLE;
        }
    }

    void GLFWWindow::ImageData::_clear() {
        device = VK_NULL_HANDLE;

        image = VK_NULL_HANDLE;
        view = VK_NULL_HANDLE;
        command_buffer = VK_NULL_HANDLE;
        descriptor = VK_NULL_HANDLE;
        framebuffer = VK_NULL_HANDLE;
        ready_fence = VK_NULL_HANDLE;
    }


    GLFWWindow::GLFWWindow() {
        log = getOrCreateLogger("Window");
    }

    GLFWWindow::~GLFWWindow() {
    }

    void GLFWWindow::preInit(render_engine::VulkanInterface* vulkan) {
        this->vulkan = vulkan;

        render_engine::QueueRequest request{};
        request.flags = 0;
        request.priority = 1.f;
        request.queue_suitability_hook = std::bind(&GLFWWindow::_queueSuitabilityHook, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);

        queue_id = vulkan->requestQueue(request);
    }

    void GLFWWindow::init() {
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        window = glfwCreateWindow(800, 600, "Magma", nullptr, nullptr);
        if(!window) {
            log->critical("Failed to create window");
            throw std::runtime_error{ "Failed to create window" };
        }

        queue = &(vulkan->getQueue(queue_id));

        _createPipelineLayout();
        _createCommandPool();
        _createSurface();

        _createSwapchain();
        _createRenderPass();
        _createPipeline();
    }

    void GLFWWindow::destroy() {
        VkDevice device{ vulkan->getDevice() };

        if(pipeline != VK_NULL_HANDLE) {
            vkDestroyPipeline(device, pipeline, nullptr);
            pipeline = VK_NULL_HANDLE;
        }

        if(render_pass != VK_NULL_HANDLE) {
            vkDestroyRenderPass(device, render_pass, nullptr);
            render_pass = VK_NULL_HANDLE;
        }

        swapchain_images.clear();

        if(swapchain != VK_NULL_HANDLE) {
            vkDestroySwapchainKHR(device, swapchain, nullptr);
            swapchain = VK_NULL_HANDLE;
        }

        if(surface != VK_NULL_HANDLE) {
            vkDestroySurfaceKHR(vulkan->getInstance(), surface, nullptr);
            surface = VK_NULL_HANDLE;
        }

        if(pipeline_layout != VK_NULL_HANDLE) {
            vkDestroyPipelineLayout(device, pipeline_layout, nullptr);
            pipeline_layout = VK_NULL_HANDLE;
        }

        if(descriptor_pool != VK_NULL_HANDLE) {
            vkDestroyDescriptorPool(device, descriptor_pool, nullptr);
            descriptor_pool = VK_NULL_HANDLE;
        }

        if(command_pool != VK_NULL_HANDLE) {
            vkDestroyCommandPool(device, command_pool, nullptr);
            command_pool = VK_NULL_HANDLE;
        }

        if(window) {
            glfwDestroyWindow(window);
            window = nullptr;
        }
    }
        
    void GLFWWindow::setFullscreen(bool enable) {

    }

    void GLFWWindow::getWindowSize(uint32_t& width, uint32_t& height) const {

    }

    void GLFWWindow::getFramebufferSize(uint32_t& width, uint32_t& height) const {

    }

    void GLFWWindow::getWindowScale(float& x_scale, float& y_scale) const {

    }

    bool GLFWWindow::isFullscreen() const {
        return false;
    }

    void GLFWWindow::registerWindowResizeHook(WindowResizeHook hook) { 

    }

    void GLFWWindow::registerFramebufferResizeHook(FramebufferResizeHook hook) {

    }

    void GLFWWindow::displayFrame(VkImageView image, VkSemaphore wait_semaphore, const std::vector<VkSemaphore>& signal_semaphores, VkFence signal_fence) {

    }
    
    void GLFWWindow::_createPipelineLayout() {
        VkPipelineLayoutCreateInfo info;
        info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        info.pNext = nullptr;
        info.flags = 0;
        info.setLayoutCount = 1;
        info.pSetLayouts = nullptr;
        info.pushConstantRangeCount = 0;
        info.pPushConstantRanges = nullptr;
    
        VkResult res{ vkCreatePipelineLayout(vulkan->getDevice(), &info, nullptr, &pipeline_layout) };
        if(res != VK_SUCCESS) {
            log->error("Failed to create pipeline layout {}", res);
            throw std::runtime_error{ "Failed to create pipeline layout" };
        }
    }

    void GLFWWindow::_createCommandPool() {
        VkCommandPoolCreateInfo info;
        info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        info.pNext = nullptr;
        info.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT | VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
        info.queueFamilyIndex = queue->getQueueFamilyIndex();

        VkResult res{ vkCreateCommandPool(vulkan->getDevice(), &info, nullptr, &command_pool) };
        if(res != VK_SUCCESS) {
            log->error("Failed to create command pool {}", res);
            throw std::runtime_error{ "Failed to create command pool" };
        }
    }

    void GLFWWindow::_createSurface() {
        VkResult res{ glfwCreateWindowSurface(vulkan->getInstance(), window, nullptr, &surface) };
        if(res != VK_SUCCESS) {
            log->error("Failed to create window surface {}", res);
            throw std::runtime_error{ "Failed to create window surface" };
        }

        VkBool32 supported;
        res = vkGetPhysicalDeviceSurfaceSupportKHR(vulkan->getPhysicalDevice(), queue->getQueueFamilyIndex(), surface, &supported);
        if(res != VK_SUCCESS || !supported) {
            log->error("Device does not support display to surface");
            throw std::runtime_error{ "Device does not support display to surface" };
        }
    }

    VkSurfaceFormatKHR GLFWWindow::_selectSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& formats) const {
        bool comp4_found{ false };
        VkSurfaceFormatKHR comp4;
        for(size_t i{ 0 }; i < formats.size(); i++) {
            if(formats[i].colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
                if(formats[i].format == VK_FORMAT_R8G8B8_UNORM) {
                    return formats[i];
                }

                if(formats[i].format == VK_FORMAT_B8G8R8_UNORM) {
                    return formats[i];
                }

                if(formats[i].format == VK_FORMAT_R8G8B8A8_UNORM) {
                    comp4 = formats[i];
                    comp4_found = true;
                }

                if(formats[i].format == VK_FORMAT_B8G8R8A8_UNORM) {
                    comp4 = formats[i];
                    comp4_found = true;
                }
            }
        }

        if(comp4_found) {
            return comp4;
        }

        return formats[0];
    }

    VkPresentModeKHR GLFWWindow::_selectPresentMode(const std::vector<VkPresentModeKHR>& modes) const {
        for(size_t i{ 0 }; i < modes.size(); i++) {
            if(modes[i] == VK_PRESENT_MODE_MAILBOX_KHR) {
                return modes[i];
            }
        }

        return VK_PRESENT_MODE_FIFO_KHR;
    }

    VkExtent2D GLFWWindow::_selectSwapExtent(const VkSurfaceCapabilitiesKHR& caps) const {
        if(caps.currentExtent.width != UINT32_MAX) {
            return caps.currentExtent;
        }

        int w, h;
        glfwGetFramebufferSize(const_cast<GLFWwindow*>(window), &w, &h);
        VkExtent2D extent{ static_cast<uint32_t>(w), static_cast<uint32_t>(h) };

        extent.width = std::clamp(extent.width, caps.minImageExtent.width, caps.maxImageExtent.width);
        extent.height = std::clamp(extent.height, caps.minImageExtent.height, caps.maxImageExtent.height);

        return extent;
    }

    void GLFWWindow::_createSwapchain() {
        swapchain_images.clear();

        VkSurfaceCapabilitiesKHR capabilities;
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(vulkan->getPhysicalDevice(), surface, &capabilities);

        if(!(capabilities.supportedCompositeAlpha & VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR)) {
            log->error("Opaque composite alpha not supported");
            throw std::runtime_error{ "Opaque composite alpha not supported" };
        }
        
        if(!(capabilities.supportedUsageFlags & VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT)) {
            log->error("Color attachement usage for swapchain not supported");
            throw std::runtime_error{ "Color attachement usage for swapchain not supported" };
        }

        display_size = _selectSwapExtent(capabilities);
        uint32_t image_count{ std::clamp(3u, capabilities.minImageCount, capabilities.maxImageCount) };

        {
            uint32_t count;
            vkGetPhysicalDeviceSurfaceFormatsKHR(vulkan->getPhysicalDevice(), surface, &count, nullptr);
            std::vector<VkSurfaceFormatKHR> formats( count );
            vkGetPhysicalDeviceSurfaceFormatsKHR(vulkan->getPhysicalDevice(), surface, &count, formats.data());
            surface_format = _selectSurfaceFormat(formats);
        }

        VkPresentModeKHR mode;
        {
            uint32_t count;
            vkGetPhysicalDeviceSurfacePresentModesKHR(vulkan->getPhysicalDevice(), surface, &count, nullptr);
            std::vector<VkPresentModeKHR> modes( count );
            vkGetPhysicalDeviceSurfacePresentModesKHR(vulkan->getPhysicalDevice(), surface, &count, modes.data());
            present_mode = _selectPresentMode(modes);
        }


        VkSwapchainKHR old_swapchain{ swapchain };

        VkSwapchainCreateInfoKHR info;
        info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        info.pNext = nullptr;
        info.flags = 0;
        info.surface = surface;
        info.minImageCount = image_count;
        info.imageFormat = surface_format.format;
        info.imageColorSpace = surface_format.colorSpace;
        info.imageExtent = display_size;
        info.imageArrayLayers = 1;
        info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
        info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        info.queueFamilyIndexCount = 0;
        info.pQueueFamilyIndices = nullptr;
        info.preTransform = capabilities.currentTransform;
        info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        info.presentMode = mode;
        info.clipped = VK_TRUE;
        info.oldSwapchain = old_swapchain;
        
        VkResult res{ vkCreateSwapchainKHR(vulkan->getDevice(), &info, nullptr, &swapchain) };

        if(old_swapchain != VK_NULL_HANDLE) {
            vkDestroySwapchainKHR(vulkan->getDevice(), old_swapchain, nullptr);
        }

        if(res != VK_SUCCESS) {
            log->error("Failed to create swapchain {}", res);
            throw std::runtime_error{ "Failed to create swapchain" };
        }

        uint32_t swapchain_image_count;
        std::vector<VkImage> images;
        vkGetSwapchainImagesKHR(vulkan->getDevice(), swapchain, &swapchain_image_count, nullptr);
        images.resize(swapchain_image_count);
        vkGetSwapchainImagesKHR(vulkan->getDevice(), swapchain, &swapchain_image_count, images.data());

        swapchain_images.reserve(swapchain_image_count);
        for(uint32_t i{ 0 }; i < swapchain_image_count; i++) {
            ImageData& image{ swapchain_images.emplace_back(vulkan->getDevice()) };

            image.image = images[i];

            _createFramebuffer(image);
            _createSyncObjects(image);
        }

        _createCommandBuffers();
    }
    
    void GLFWWindow::_createImageView(ImageData& image) {
        VkImageViewCreateInfo view_info;
        view_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        view_info.pNext = nullptr;
        view_info.flags = 0;
        view_info.image = image.image;
        view_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
        view_info.format = surface_format.format;
        view_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        view_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        view_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        view_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        view_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        view_info.subresourceRange.baseMipLevel = 0;
        view_info.subresourceRange.levelCount = 1;
        view_info.subresourceRange.baseArrayLayer = 0;
        view_info.subresourceRange.layerCount = 1;

        VkResult res{ vkCreateImageView(vulkan->getDevice(), &view_info, nullptr, &(image.view)) };
        if(res != VK_SUCCESS) {
            log->error("Failed to create image view {}", res);
            throw std::runtime_error{ "Failed to create image view" };
        }
    }

    void GLFWWindow::_createRenderPass() {

    }
    
    void GLFWWindow::_createPipeline() {

    }
    
    void GLFWWindow::_createFramebuffer(ImageData& image) {
        VkFramebufferCreateInfo info;
        info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        info.pNext = nullptr;
        info.flags = 0;
        info.renderPass = render_pass;
        info.attachmentCount = 1;
        info.pAttachments = &image.view;
        info.width = display_size.width;
        info.height = display_size.height;
        info.layers = 1;

        VkResult res{ vkCreateFramebuffer(vulkan->getDevice(), &info, nullptr, &image.framebuffer) };
        if(res != VK_SUCCESS) {
            log->error("Failed to create framebuffer {}", res);
            throw std::runtime_error{ "Failed to create framebuffer" };
        }
    }

    void GLFWWindow::_createSyncObjects(ImageData& image) {
        VkFenceCreateInfo info;
        info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        info.pNext = nullptr;
        info.flags = VK_FENCE_CREATE_SIGNALED_BIT;

        VkResult res{ vkCreateFence(vulkan->getDevice(), &info, nullptr, &image.ready_fence) };
        if(res != VK_SUCCESS) {
            log->error("Failed to create fence {}", res);
            throw std::runtime_error{ "Failed to create fence" };
        }
    }

    void GLFWWindow::_createCommandBuffers() {
        VkResult res{ vkResetCommandPool(vulkan->getDevice(), command_pool, 0) };
        if(res != VK_SUCCESS) {
            log->error("Failed to reset command pool {}", res);
            throw std::runtime_error{ "Failed to reset command pool" };
        }

        VkCommandBufferAllocateInfo info;
        info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        info.pNext = nullptr;
        info.commandPool = command_pool;
        info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        info.commandBufferCount = static_cast<uint32_t>(swapchain_images.size());

        std::vector<VkCommandBuffer> buffers(swapchain_images.size());
        res = vkAllocateCommandBuffers(vulkan->getDevice(), &info, buffers.data());
        if(res != VK_SUCCESS) {
            log->error("Failed to allocate command buffers {}", res);
            throw std::runtime_error{ "Failed to allocate command buffers" };
        }

        for(uint32_t i{ 0 }; i < buffers.size(); i++) {
            swapchain_images[i].command_buffer = buffers[i];
        }
    }
    
    bool GLFWWindow::_queueSuitabilityHook(render_engine::VulkanInterface& interface, const render_engine::PhysicalDeviceInfo& info, uint32_t family) {
        if(glfwGetPhysicalDevicePresentationSupport(interface.getInstance(), info.device, family) == GLFW_FALSE) {
            return false;
        } else {
            return true;
        }
    }
}