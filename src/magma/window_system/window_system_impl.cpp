#include "window_system_impl.hpp"

#include "magma/magma.hpp"
#include "magma/render_engine/render_engine.hpp"

namespace magma::window_system {
    
    WindowSystem_impl::WindowSystem_impl(WindowSystem& sys) : instance{ sys } {
        log = getOrCreateLogger("WindowSystem");
        glfw_log = getOrCreateLogger("GLFW");
    }

    WindowSystem_impl::~WindowSystem_impl() {
    }

    void WindowSystem_impl::preInit() {
        log->debug("Pre init started");

        window.preInit(&instance.getMagma().getRenderEngine().getVulkan());
    
        log->debug("Pre init complete");
    }

    void WindowSystem_impl::init() {
        log->debug("Init started");

        int res{ glfwInit() };
        if(res != GLFW_TRUE) {
            log->critical("Failed to initialize GLFW {}", res);
            throw std::runtime_error{ "Failed to initialize GLFW" };
        }

        glfwSetErrorCallback(&_glfwErrorCallback);

        window.init();

        log->debug("Init complete");
    }

    void WindowSystem_impl::destroy() {
        log->debug("Destroy started");

        window.destroy();
        glfwTerminate();

        log->debug("Destroy complete");
    }

    Logger WindowSystem_impl::glfw_log;
    void WindowSystem_impl::_glfwErrorCallback(int, const char* msg) {
        glfw_log->error("{}", msg);
    }
}