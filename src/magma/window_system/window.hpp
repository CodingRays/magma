#pragma once
#include "magma/include.hpp"

#include <vector>
#include <functional>

namespace magma {
    class WindowSystem;
}

namespace magma::window_system {

    class Window;

    using WindowResizeHook = std::function<void(Window&, uint32_t, uint32_t)>;
    using FramebufferResizeHook = std::function<void(Window&, uint32_t, uint32_t)>;

    class Window {
    public:
        virtual void setFullscreen(bool enable) = 0;

        virtual void getWindowSize(uint32_t& width, uint32_t& height) const = 0;
        virtual void getFramebufferSize(uint32_t& width, uint32_t& height) const = 0;
        virtual void getWindowScale(float& x_scale, float& y_scale) const = 0;

        virtual bool isFullscreen() const = 0;

        virtual void registerWindowResizeHook(WindowResizeHook hook) = 0;
        virtual void registerFramebufferResizeHook(FramebufferResizeHook hook) = 0;
    };
}
