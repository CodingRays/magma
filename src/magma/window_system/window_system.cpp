#include "window_system.hpp"
#include "window_system_impl.hpp"

namespace magma {

    WindowSystem::WindowSystem(Magma& engine) : CoreModule{ engine, "WindowSystem" } {
        instance = std::make_unique<window_system::WindowSystem_impl>(*this);
    }

    WindowSystem::~WindowSystem() {
    }

    void WindowSystem::preInit() {
        instance->preInit();
    }

    void WindowSystem::init() {
        instance->init();
    }

    void WindowSystem::destroy() {
        instance->destroy();
    }
    
    window_system::Window& WindowSystem::getWindow() {
        return instance->window;
    }
}