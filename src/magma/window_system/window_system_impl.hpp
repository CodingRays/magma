#pragma once
#include "magma/include.hpp"

#include "window_system.hpp"
#include "glfw_window.hpp"

namespace magma::window_system {

    class WindowSystem_impl {
    public:
        WindowSystem_impl(WindowSystem& sys);
        ~WindowSystem_impl();

        void preInit();
        void init();

        void destroy();

        static Logger glfw_log;
        static void _glfwErrorCallback(int, const char*);

        WindowSystem& instance;
        GLFWWindow window;

        Logger log;
    };
}