#pragma once
#include "magma/include.hpp"

#include "magma/core_module.hpp"

#include <memory>

namespace magma {

    class Magma;

    namespace window_system {
        class WindowSystem_impl;
        class Window;
    }

    class WindowSystem : public CoreModule {
    public:
        WindowSystem(Magma& engine);
        ~WindowSystem();

        void preInit() override;
        void init() override;

        void destroy() override;

        window_system::Window& getWindow();

    private:
        std::unique_ptr<window_system::WindowSystem_impl> instance;
    };
}