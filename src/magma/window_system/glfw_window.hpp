#pragma once
#include "magma/include.hpp"

#include "include_glfw.hpp"

#include "window.hpp"

#include "magma/render_engine/include_vulkan.hpp"
#include "magma/render_engine/vulkan_interface.hpp"
#include "magma/render_engine/display_provider.hpp"

#include <mutex>
#include <vector>

namespace magma::window_system {

    class GLFWWindow : public Window, public render_engine::DisplayProvider {
    public:
        GLFWWindow();
        ~GLFWWindow();

        void preInit(render_engine::VulkanInterface* vulkan);
        void init();

        void destroy();

        void update();
        
        virtual void setFullscreen(bool enable) override;

        virtual void getWindowSize(uint32_t& width, uint32_t& height) const override;
        virtual void getFramebufferSize(uint32_t& width, uint32_t& height) const override;
        virtual void getWindowScale(float& x_scale, float& y_scale) const override;

        virtual bool isFullscreen() const override;

        virtual void registerWindowResizeHook(WindowResizeHook hook) override;
        virtual void registerFramebufferResizeHook(FramebufferResizeHook hook) override;

        virtual void displayFrame(VkImageView image, VkSemaphore wait_semaphore, const std::vector<VkSemaphore>& signal_semaphores, VkFence signal_fence) override;

    private:
        // Represents metadata for one swapchain image.
        class ImageData {
        public:
            ImageData(VkDevice device);
            ImageData(const ImageData&) = delete;
            ImageData(ImageData&&);

            ~ImageData();

            ImageData& operator=(const ImageData&) = delete;
            ImageData& operator=(ImageData&&);

            VkImage image{ VK_NULL_HANDLE };
            VkImageView view{ VK_NULL_HANDLE };

            VkCommandBuffer command_buffer{ VK_NULL_HANDLE };
            VkDescriptorSet descriptor{ VK_NULL_HANDLE };

            VkFramebuffer framebuffer{ VK_NULL_HANDLE };

            VkFence ready_fence{ VK_NULL_HANDLE };
        
        private:
            void _delete();
            void _clear();
        
            VkDevice device{ VK_NULL_HANDLE };
        };

        void _createPipelineLayout();
        void _createCommandPool();
        void _createSurface();

        VkSurfaceFormatKHR _selectSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& formats) const;
        VkPresentModeKHR _selectPresentMode(const std::vector<VkPresentModeKHR>& modes) const;
        VkExtent2D _selectSwapExtent(const VkSurfaceCapabilitiesKHR& caps) const;

        void _createSwapchain();
        void _createRenderPass();
        void _createPipeline();

        void _createImageView(ImageData& image);
        void _createFramebuffer(ImageData& image);
        void _createSyncObjects(ImageData& image);
        void _createCommandBuffers();

        bool _queueSuitabilityHook(render_engine::VulkanInterface&, const render_engine::PhysicalDeviceInfo&, uint32_t);

        void _dispatchWindowResizeHook(uint32_t width, uint32_t height);
        void _dispatchFramebufferResizeHook(uint32_t width, uint32_t height);

        Logger log;
        render_engine::VulkanInterface* vulkan{ nullptr };

        GLFWwindow* window{ nullptr };

        VkExtent2D display_size;
        VkSurfaceFormatKHR surface_format;
        VkPresentModeKHR present_mode;

        uint32_t queue_id;
        render_engine::VulkanQueue* queue{ nullptr };

        VkCommandPool command_pool{ VK_NULL_HANDLE };
        VkDescriptorPool descriptor_pool{ VK_NULL_HANDLE };

        VkPipelineLayout pipeline_layout{ VK_NULL_HANDLE };
        
        VkSurfaceKHR surface{ VK_NULL_HANDLE };
        VkSwapchainKHR swapchain{ VK_NULL_HANDLE };

        std::vector<ImageData> swapchain_images;

        VkRenderPass render_pass{ VK_NULL_HANDLE };
        VkPipeline pipeline{ VK_NULL_HANDLE };

        bool rebuild_needed{ false };
        std::mutex vulkan_mutex;

        std::recursive_mutex hook_mutex; // TODO be smarter
        std::vector<WindowResizeHook> window_resize_hooks;
        std::vector<FramebufferResizeHook> framebuffer_resize_hooks;
    };
}