#pragma once
#include "include.hpp"

#include <memory>

namespace magma {

    Logger getOrCreateLogger(const std::string& name);

    namespace detail {
        class Magma_impl;
    }

    class RenderEngine;

    class Magma {
    public:
        Magma();
        ~Magma();

        void init();
        void destroy();

        RenderEngine& getRenderEngine();

    private:

        std::unique_ptr<detail::Magma_impl> instance;
    };
}