#pragma once
#include <spdlog/spdlog.h>

#include "include_glfw_vulkan.hpp"

using Logger = std::shared_ptr<spdlog::logger>;

namespace magma {

#define MAGMA_VERSION_MAJOR 0
#define MAGMA_VERSION_MINOR 2

    Logger getOrCreateLogger(const std::string& name);

    using RID = uint32_t;
    RID createRID();
}