#pragma once
#include "include.hpp"

#include <atomic>
#include <vector>

namespace magma {

    class VulkanBackend;
    class ShaderRegistry;

    class DisplayEngine {
    public:
        enum class State : uint32_t {
            UNINITIALIZED,
            INIT_STARTED,
            INITIALIZED,
            DESTROY_STARTED,
            FAILED,
        };

        ~DisplayEngine();

        void init(VulkanBackend& backend, ShaderRegistry& registry);
        void destroy();

        void update();

        /**
         * Draws the content of src on the screen.
         * 
         * If wait is not VK_NULL_HANLDE waits for wait to be signaled before
         * reading src.
         * 
         * src must be in the VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL layout.
         */
        void drawFrame(VkImageView src, VkSemaphore wait);

        void enqueueRebuild();
        VkExtent2D getDisplaySize() const;

    private:
        void _destroy();

        void _rebuildSwapchain();

        void _createDescriptorSetLayout();
        void _createSampler();
        void _createPipelineLayout();
        void _createCommandPool();
        void _createSurface();

        VkSurfaceFormatKHR _selectSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& formats) const;
        VkPresentModeKHR _selectPresentMode(const std::vector<VkPresentModeKHR>& modes) const;
        VkExtent2D _selectSwapExtent(const VkSurfaceCapabilitiesKHR& caps) const;

        void _createSwapchain();
        
        void _createRenderPass();
        void _createPipeline();
        void _createFramebuffers();
        void _createDescriptorSets();

        void _updatePerImageSyncObjects();
        void _updateCommandBuffers();

        void _prepareSwapchainRecreate();

        Logger log;

        std::atomic<State> state{ State::UNINITIALIZED };
        VkExtent2D display_size;
        VkSurfaceFormatKHR surface_format;

        VulkanBackend* backend;
        ShaderRegistry* registry;

        VkQueue display_queue{ VK_NULL_HANDLE };
        VkCommandPool cmd_pool{ VK_NULL_HANDLE };
        std::vector<VkCommandBuffer> cmd_buffers;

        VkSampler sampler{ VK_NULL_HANDLE };

        VkSurfaceKHR surface{ VK_NULL_HANDLE };

        VkSwapchainKHR swapchain{ VK_NULL_HANDLE };
        uint32_t swapchain_image_count;
        std::vector<VkImage> swapchain_images;
        std::vector<VkImageView> swapchain_views;

        VkDescriptorSetLayout set_layout{ VK_NULL_HANDLE };
        VkPipelineLayout layout{ VK_NULL_HANDLE };

        VkDescriptorPool descriptor_pool{ VK_NULL_HANDLE };
        std::vector<VkDescriptorSet> descriptor_sets;
        VkRenderPass render_pass{ VK_NULL_HANDLE };
        VkPipeline pipeline{ VK_NULL_HANDLE };
        std::vector<VkFramebuffer> framebuffers;

        std::vector<VkSemaphore> draw_done_semaphores;
        std::vector<VkSemaphore> image_ready_semaphores;

        VkFence draw_done_fence{ VK_NULL_HANDLE };

        uint32_t draw_iteration{ 0 };
        bool rebuild_needed{ false };
    };
}