#include "include.hpp"

#include <string>
#include <map>
#include <memory>
#include <mutex>
#include <vector>

namespace magma {
    
    class VulkanBackend;

    class ShaderEntryPoint {
    public:
        ShaderEntryPoint(VkShaderModule module, std::string&& entry, VkShaderStageFlagBits stages);

        VkShaderModule getShaderModule() const;
        const std::string& getEntryPointName() const;
        VkShaderStageFlagBits getShaderStage() const;

        void configureShaderStageInfo(VkPipelineShaderStageCreateInfo& info) const;
        void configureShaderStageInfo(VkPipelineShaderStageCreateInfo& info, VkSpecializationInfo* specialization) const;

    private:
        VkShaderModule module;
        std::string entry;
        VkShaderStageFlagBits stage;
    };

    class ShaderRegistry {
    public:
        ~ShaderRegistry();

        void init(VulkanBackend& backend);
        void destroy();

        void registerShaderModule(const std::string& group, const uint32_t* data, uint32_t data_size);
        void registerShaderModule(const std::string& group, const std::string& path);

        const ShaderEntryPoint* findEntryPoint(const std::string& name) const;
        const ShaderEntryPoint* findEntryPoint(const std::string& group, const std::string& name) const;

        std::vector<const ShaderEntryPoint*> listEntryPoints(const std::string& group) const;

    private:
        class EntryPointGroup {
        public:
            ~EntryPointGroup();

            bool addEntryPoint(std::unique_ptr<ShaderEntryPoint>&& entry_point);
            const ShaderEntryPoint* findEntryPoint(const std::string& name) const;

            std::vector<const ShaderEntryPoint*> listEntryPoints() const;

        private:
            std::map<std::string, std::unique_ptr<ShaderEntryPoint>> entry_points;
        };

        void _destroy();

        VkShaderStageFlagBits _convertShaderModelToStage(uint32_t shader_model);

        void _parseShaderModule(const std::string& group, const uint32_t* data, uint32_t data_size);
        void _loadShaderModule(const std::string& path, std::vector<uint32_t>& data) const;

        const EntryPointGroup* _findGroup(const std::string& name) const;


    	VulkanBackend* backend{ nullptr };

        std::vector<VkShaderModule> shader_modules;
        std::map<std::string, EntryPointGroup> groups;

        Logger log;
        std::mutex mutex;
    };
}