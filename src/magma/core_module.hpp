#pragma once
#include "include.hpp"

#include <atomic>
#include <cstdint>
#include <string>

namespace magma {

    class Magma;

    class CoreModule {
    public:
        using StateType = uint32_t;

    public:
        virtual ~CoreModule();
        
        virtual const std::string& getModuleName() const final;

        virtual Magma& getMagma() const final;

        virtual void preInit() = 0;
        virtual void init() = 0;

        virtual void destroy() = 0;

    protected:
        CoreModule(Magma& engine, const char* module_name);

        Magma& engine;
        Logger log;

        const std::string module_name;
    };
}
