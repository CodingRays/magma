#pragma once

#include "include.hpp"

#define VMA_STATIC_VULKAN_FUNCTIONS 1
#include <vk_mem_alloc.h>

#include <array>
#include <atomic>
#include <vector>

namespace magma {

    class VulkanBackend {
    public:
        enum class State : uint32_t {
            UNINITIALIZED,
            INIT_STARTED,
            INITIALIZED,
            DESTROY_STARTED,
            FAILED,
        };

        enum class QueueId {
            RENDER,
            DISPLAY,
            COMPUTE,
            TRANSFER,
            QUEUE_ID_SIZE
        };

        static constexpr size_t kQueueIdCount{ static_cast<size_t>(QueueId::QUEUE_ID_SIZE) };

        ~VulkanBackend();

        void init(Logger log);
        void destroy();

        inline State getState() const;

        inline VkInstance getInstance();
        inline VkPhysicalDevice getPhysicalDevice();
        inline VkDevice getDevice();
        inline VmaAllocator getAllocator();

        inline GLFWwindow* getWindow();

        VkQueue getQueue(QueueId queue_id);
        uint32_t getQueueFamily(QueueId queue_id);

    private:
        struct QueueInfo {
            uint32_t family;
            uint32_t index;
            VkQueue queue{ VK_NULL_HANDLE };
        };

        struct PhysicalDeviceInfo {
            VkPhysicalDevice device{ VK_NULL_HANDLE };
            VkPhysicalDeviceProperties properties;
            VkPhysicalDeviceFeatures features;
            std::array<QueueInfo, kQueueIdCount> queues;

            // The number of instances required of a queue family
            std::vector<uint32_t> queue_count;
        };

        void _destroy();

        void _initGLFW();
        void _createInstance();
        void _createDevice();
        void _createAllocator();
        void _createWindow();

        int _ratePhysicalDevice(
            PhysicalDeviceInfo& device, 
            const std::vector<const char*>& required_extensions, 
            const std::vector<const char*>& required_layers);

        void _collectInstanceExtensions(std::vector<const char*>& extensions);
        void _collectInstanceLayers(std::vector<const char*>& layers);

        void _collectDeviceExtensions(std::vector<const char*>& extensions);
        void _collectDeviceLayers(std::vector<const char*>& layers);

        std::atomic<State> state{ State::UNINITIALIZED };
        bool glfw_initialized{ false };
        Logger log;

        VkInstance instance{ VK_NULL_HANDLE };
        VkPhysicalDevice physical_device{ VK_NULL_HANDLE };
        VkDevice device{ VK_NULL_HANDLE };

        VmaAllocator allocator{ nullptr };

        std::array<QueueInfo, kQueueIdCount> queues;

        GLFWwindow* window{ nullptr };

        static constexpr bool debug_vulkan{ true };
        VkDebugUtilsMessengerEXT debug_messenger;
        PFN_vkDestroyDebugUtilsMessengerEXT debug_destroy{ nullptr };

        static Logger vk_log;
        static VKAPI_ATTR VkBool32 VKAPI_CALL _vulkanDebugCb(
            VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
            VkDebugUtilsMessageTypeFlagsEXT messageType,
            const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
            void* pUserData);

        static Logger glfw_log;
        static void _glfwErrorCb(int code, const char* msg);
    };

    // Inline function definitions

    VulkanBackend::State VulkanBackend::getState() const {
        return state;
    }

    VkInstance VulkanBackend::getInstance() {
        return instance;
    }

    VkPhysicalDevice VulkanBackend::getPhysicalDevice() {
        return physical_device;
    }

    VkDevice VulkanBackend::getDevice() {
        return device;
    }

    VmaAllocator VulkanBackend::getAllocator() {
        return allocator;
    }

    GLFWwindow* VulkanBackend::getWindow() {
        return window;
    }
}