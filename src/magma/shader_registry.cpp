#include "shader_registry.hpp"

#include "backend.hpp"

#include <vulkan/spirv.hpp11>

#include <fstream>
#include <limits>

namespace magma {

    // TODO thread safety

    ShaderEntryPoint::ShaderEntryPoint(VkShaderModule module, std::string&& entry, VkShaderStageFlagBits stage) :
        module{ module },
        entry{ std::move(entry) }, 
        stage{ stage } {
    }

    VkShaderModule ShaderEntryPoint::getShaderModule() const {
        return module;
    }

    const std::string& ShaderEntryPoint::getEntryPointName() const {
        return entry;
    }

    VkShaderStageFlagBits ShaderEntryPoint::getShaderStage() const {
        return stage;
    }

    void ShaderEntryPoint::configureShaderStageInfo(VkPipelineShaderStageCreateInfo& info) const {
        configureShaderStageInfo(info, nullptr);
    }

    void ShaderEntryPoint::configureShaderStageInfo(VkPipelineShaderStageCreateInfo& info, VkSpecializationInfo* specialization) const {
        info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        info.pNext = nullptr;
        info.flags = 0;
        info.stage = stage;
        info.module = module;
        info.pName = entry.c_str();
        info.pSpecializationInfo = specialization;
    }

    ShaderRegistry::~ShaderRegistry() {
        _destroy();
    }

    void ShaderRegistry::init(VulkanBackend& backend) {
        this->backend = &backend;
        log = getOrCreateLogger("ShaderRegistry");
        if(!log) {
            throw std::runtime_error{ "Unable to create shader registry logger" };
        }
        log->set_level(spdlog::level::trace);
    }

    void ShaderRegistry::destroy() {
        _destroy();
    }

    void ShaderRegistry::registerShaderModule(const std::string& group, const uint32_t* data, uint32_t data_size) {
        _parseShaderModule(group, data, data_size);
    }

    void ShaderRegistry::registerShaderModule(const std::string& group, const std::string& path) {
        std::vector<uint32_t> data;
        _loadShaderModule(path, data);

        size_t data_size{ data.size() * sizeof(uint32_t) };
        if(data_size > std::numeric_limits<uint32_t>::max()) {
            log->error("Shader module loaded from \"{}\" is too big", path);
            throw std::runtime_error{ "Shader module is too big" };
        }

        _parseShaderModule(group, data.data(), static_cast<uint32_t>(data_size));
    }

    const ShaderEntryPoint* ShaderRegistry::findEntryPoint(const std::string& name) const {
        std::string::size_type sep_index{ name.find(':') };
        if(sep_index == std::string::npos) {
            log->error("Invalid entry point name \"{}\". No \':\' found", name);
            throw std::runtime_error{ "Invalid entry point name" };
        }

        std::string group{ name.substr(0, sep_index) };
        std::string entry_name{ name.substr(sep_index + 1, std::string::npos) };
        return findEntryPoint(group, entry_name);
    }

    const ShaderEntryPoint* ShaderRegistry::findEntryPoint(const std::string& group, const std::string& name) const {
        const EntryPointGroup* g{ _findGroup(group) };
        if(g) {
            return g->findEntryPoint(name);
        } else {
            return nullptr;
        }
    }

    std::vector<const ShaderEntryPoint*> ShaderRegistry::listEntryPoints(const std::string& group) const {
        const EntryPointGroup* g{ _findGroup(group) };
        if(g) {
            return g->listEntryPoints();
        } else {
            return std::vector<const ShaderEntryPoint*>{};
        }
    }

    ShaderRegistry::EntryPointGroup::~EntryPointGroup() {
    }

    bool ShaderRegistry::EntryPointGroup::addEntryPoint(std::unique_ptr<ShaderEntryPoint>&& entry_point) {
        auto it{ entry_points.find(entry_point->getEntryPointName()) };
        if(it != entry_points.end()) {
            return false;
        }

        std::string name{ entry_point->getEntryPointName() };
        entry_points.emplace(std::move(name), std::move(entry_point));
        return true;
    }

    const ShaderEntryPoint* ShaderRegistry::EntryPointGroup::findEntryPoint(const std::string& name) const {
        auto it{ entry_points.find(name) };
        if(it == entry_points.end()) {
            return nullptr;
        }

        return it->second.get();
    }

    std::vector<const ShaderEntryPoint*> ShaderRegistry::EntryPointGroup::listEntryPoints() const {
        std::vector<const ShaderEntryPoint*> ret;
        ret.reserve(entry_points.size());

        for(auto& entry : entry_points) {
            ret.push_back(entry.second.get());
        }

        return ret;
    }

    void ShaderRegistry::_destroy() {
        log->debug("Destroying ShaderRegistry");

        for(VkShaderModule module : shader_modules) {
            vkDestroyShaderModule(backend->getDevice(), module, nullptr);
        }

        shader_modules.clear();
        groups.clear();
    }

    VkShaderStageFlagBits ShaderRegistry::_convertShaderModelToStage(uint32_t shader_model) {
        spv::ExecutionModel model{ static_cast<spv::ExecutionModel>(shader_model) };
        switch(model) {
            case spv::ExecutionModel::Vertex:
            return VK_SHADER_STAGE_VERTEX_BIT;

            case spv::ExecutionModel::TessellationControl:
            return VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;

            case spv::ExecutionModel::TessellationEvaluation:
            return VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;

            case spv::ExecutionModel::Geometry:
            return VK_SHADER_STAGE_GEOMETRY_BIT;

            case spv::ExecutionModel::Fragment:
            return VK_SHADER_STAGE_FRAGMENT_BIT;

            case spv::ExecutionModel::GLCompute:
            case spv::ExecutionModel::Kernel:
            return VK_SHADER_STAGE_COMPUTE_BIT;

            case spv::ExecutionModel::TaskNV:
            return VK_SHADER_STAGE_TASK_BIT_NV;

            case spv::ExecutionModel::MeshNV:
            return VK_SHADER_STAGE_MESH_BIT_NV;

            case spv::ExecutionModel::RayGenerationNV:
            return VK_SHADER_STAGE_RAYGEN_BIT_NV;

            case spv::ExecutionModel::IntersectionNV:
            return VK_SHADER_STAGE_INTERSECTION_BIT_NV;

            case spv::ExecutionModel::AnyHitNV:
            return VK_SHADER_STAGE_ANY_HIT_BIT_NV;

            case spv::ExecutionModel::ClosestHitNV:
            return VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV;

            case spv::ExecutionModel::MissNV:
            return VK_SHADER_STAGE_MISS_BIT_NV;

            case spv::ExecutionModel::CallableNV:
            return VK_SHADER_STAGE_CALLABLE_BIT_NV;

            default:
            throw std::runtime_error{ "Unknown execution model" };
        }
    }

    void ShaderRegistry::_parseShaderModule(const std::string& group, const uint32_t* data, uint32_t data_size) {
        VkShaderModule module{ VK_NULL_HANDLE }; 
        { // Create shader module first to verify code
            VkShaderModuleCreateInfo info;
            info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = 0;
            info.pCode = data;
            info.codeSize = data_size;

            VkResult res{ vkCreateShaderModule(backend->getDevice(), &info, nullptr, &module) };
            if(res != VK_SUCCESS) {
                log->error("Failed to create shader module {}", res);
                throw std::runtime_error{ "Failed to create shader module" };
            }
        }
        shader_modules.push_back(module);

        std::vector<std::unique_ptr<ShaderEntryPoint>> entry_points;

        uint32_t head{ 5 };
        bool done{ false };
        while(!done && head < data_size) {
            uint16_t word_count = static_cast<uint16_t>(data[head] >> spv::WordCountShift);
            spv::Op op_code = static_cast<spv::Op>(data[head] & static_cast<uint32_t>(spv::OpCodeMask));

            switch(op_code) {
                case spv::Op::OpEntryPoint: {
                    VkShaderStageFlagBits shader_stage{ _convertShaderModelToStage(data[head + 1]) };
                    std::string entry_name{ reinterpret_cast<const char*>(data + head + 3) };

                    entry_points.emplace_back(std::make_unique<ShaderEntryPoint>(module, std::move(entry_name), shader_stage));
                } break;

                case spv::Op::OpFunction:
                done = true;
                break;

                default:
                break;
            }

            head += word_count;
        }

        if(!done) {
            log->error("Failed to parse shader code. Unexpected end of data");
            throw std::runtime_error{ "Failed to parse shader code. Unexpected end of data" };
        }

        EntryPointGroup& g{ groups[group] };
        for(auto& entry_point : entry_points) {
            log->debug("Registering shader module \"{}:{}\"", group, entry_point->getEntryPointName());
            if(!g.addEntryPoint(std::move(entry_point))) {
                log->warn("Entry point with same name already registered \"{}\"", entry_point->getEntryPointName());
            }
        }
    }

    void ShaderRegistry::_loadShaderModule(const std::string& path, std::vector<uint32_t>& data) const {
        std::ifstream in{ path, std::ios::ate | std::ios::binary };
        if(!in) {
            log->error("Failed to load shader module from file \"{}\"", path);
            throw std::runtime_error{ "Failed to load shader module from file" };
        }

        std::vector<uint32_t>::size_type size{ static_cast<std::vector<uint32_t>::size_type>(in.tellg()) };
        std::vector<uint32_t>::size_type size_uint32{ size / sizeof(uint32_t) };
        
        if(size_uint32 * sizeof(uint32_t) != size) {
            log->warn("Shader module file \"{}\" is not purely word sized", path);
            size_uint32++;
        }

        data.resize(size_uint32);
        data.back() = 0;

        in.seekg(0);
        in.read(reinterpret_cast<char*>(data.data()), size);

        in.close();
    }

    const ShaderRegistry::EntryPointGroup* ShaderRegistry::_findGroup(const std::string& name) const {
        auto it{ groups.find(name) };
        if(it != groups.cend()) {
            return &(it->second);
        }
        return nullptr;
    }
}