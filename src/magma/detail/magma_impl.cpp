#include "magma_impl.hpp"

namespace magma::detail {

    Magma_impl::Magma_impl(Magma& engine) : renderer{ engine }, window{ engine } {
    }

    Magma_impl::~Magma_impl() {
        if(state != State::UNINITIALIZED) {
            log->error("Magma_impl destructor called on still initialized instance");
        }
    }

    void Magma_impl::init() {
        {
            State expected{ State::UNINITIALIZED };
            if(!state.compare_exchange_strong(expected, State::INIT_STARTED)) {
                throw std::runtime_error{ "Concurrent calls to Magma::init" };
            }
        }

        log = getOrCreateLogger("Magma");
        if(!log) {
            throw std::runtime_error{ "Failed to create magma logger" };
        }

        log->info("Magma version {}.{}", MAGMA_VERSION_MAJOR, MAGMA_VERSION_MINOR);
        log->info("Starting initialization");

        try {
            log->debug("Starting component pre init");
            state.store(State::COMPONENT_PRE_INIT);
            renderer.preInit();
            window.preInit();
            log->debug("Component pre init complete");

            // TODO

            log->debug("Starting component init");
            state.store(State::COMPONENT_INIT);
            renderer.init();
            window.init();
            log->debug("Component init complete");

        } catch(std::system_error& err) {
            log->critical("System error during initialization: {}", err.what());
            throw;
        } catch(std::runtime_error& err) {
            log->critical("Runtime error during initialization: {}", err.what());
            throw;
        } catch(...) {
            log->critical("Unknown error during initialization");
            throw;
        }

        state.store(State::INITIALIZED);
        log->info("Initialization complete");
    }

    void Magma_impl::destroy() {
        State previous_state{ state.load() };
        if(previous_state == State::DESTROY_STARTED) {
            log->critical("Called Magma_impl::destroy() while already in destroy state. Is there concurrent access?");
            return; // Cant throw since possible destructor
        }
        if(!state.compare_exchange_strong(previous_state, State::DESTROY_STARTED)) {
            log->critical("Failed to change state during Magma_impl::destroy(). Is there concurrent access?");
            return;
        }
        log->info("Destruction started");

        if(previous_state == State::INITIALIZED || previous_state == State::COMPONENT_INIT) {
            renderer.destroy();
            window.destroy();
        }

        state.store(State::UNINITIALIZED);
        log->info("Destruction complete");
    }
}