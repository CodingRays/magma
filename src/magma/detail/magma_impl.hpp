#pragma once
#include "magma/include.hpp"

#include "magma/magma.hpp"

#include "magma/render_engine/render_engine.hpp"
#include "magma/window_system/window_system.hpp"

namespace magma::detail {

    class Magma_impl {
    public:
        enum class State : uint32_t {
            UNINITIALIZED,
            INIT_STARTED,
            COMPONENT_PRE_INIT,
            COMPONENT_INIT,
            INITIALIZED,
            DESTROY_STARTED,
        };

        Magma_impl(Magma& engine);
        ~Magma_impl();

        void init();
        void destroy();

        State getState() const;

        RenderEngine renderer;
        WindowSystem window;

        Logger log;
        std::atomic<State> state{ State::UNINITIALIZED };
    };
}