#include "render_engine.hpp"
#include "render_engine_impl.hpp"

#include "display_provider.hpp"

namespace magma {
    RenderEngine::RenderEngine(Magma& engine) : CoreModule{ engine, "RenderEngine" } {
        instance = std::make_unique<render_engine::RenderEngine_impl>(*this);
    }

    RenderEngine::~RenderEngine() {
    }

    void RenderEngine::preInit() {
        instance->preInit();
    }

    void RenderEngine::init() {
        instance->init();
    }

    void RenderEngine::destroy() {
        instance->destroy();
    }

    render_engine::VulkanInterface& RenderEngine::getVulkan() {
        return *instance;
    }

    void RenderEngine::setDisplayProvider(render_engine::DisplayProvider* display_provider) {
        instance->display_provider = display_provider; // TODO synchronization, access control etc.
    }
}