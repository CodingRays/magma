#pragma once
#include "magma/include.hpp"
#include "render_engine.hpp"

#include "include_vulkan.hpp"
#include "vulkan_interface.hpp"
#include "display_provider.hpp"

namespace magma::render_engine {

    class RenderEngine_impl : public VulkanInterface {
    public:
        enum class State : uint32_t {
            UNINITIALIZED,

            PRE_INIT_STARTED,
            PRE_INIT_COMPLETE,

            INIT_STARTED,
            PRE_INSTANCE_CREATE,
            INSTANCE_CREATE,

            PRE_DEVICE_SELECTION,
            DEVICE_SELECTION,
            DEVICE_CREATE,
            POST_DEVICE_CREATE,

            INITIALIZED,

            DESTROY_STARTED,
        };

        struct PhysicalDeviceMeta {
            struct QueueFamilyMeta {
                uint32_t family;
                uint32_t count;
                std::vector<float> priorities;
            };
            
            struct QueueAssignment {
                uint32_t family_index;
                uint32_t sub_index;
            };

            PhysicalDeviceMeta();
            PhysicalDeviceMeta(VkPhysicalDevice device);

            PhysicalDeviceInfo info;
            PhysicalDeviceRequests requests;
            
            std::vector<QueueFamilyMeta> queue_metas;
            std::vector<QueueAssignment> queue_assignments;

            std::set<std::string> enabled_extensions;

            VkPhysicalDeviceFeatures enabled_features{};
            
            bool suitable;
            int rating;
        };

        struct InitData {            
            std::vector<QueueRequest> requested_queues;

            std::set<std::string> required_instance_layers;
            std::set<std::string> optional_instance_layers;

            std::set<std::string> required_instance_extensions;
            std::set<std::string> optional_instance_extensions;

            std::vector<PhysicalDeviceMeta> physical_devices;
            uint32_t selected_device_index;

            std::set<std::string> required_device_extensions;
            std::set<std::string> optional_device_extensions;

            VkPhysicalDeviceFeatures required_features{};
            VkPhysicalDeviceFeatures optional_features{};

            std::vector<PreInstanceCreateHook> pre_instance_create_hooks;
            std::vector<PreDeviceSelectionHook> pre_device_selection_hooks;
            std::vector<DeviceSelectionHook> device_selection_hooks;
            std::vector<DeviceSuitabilityHook> device_suitability_hooks;
            std::vector<PostDeviceCreateHook> post_device_create_hooks;
        };

        RenderEngine_impl(RenderEngine& instance);
        ~RenderEngine_impl();

        void preInit();
        void init();

        void destroy();

        VkInstance getInstance() const override;
        VkPhysicalDevice getPhysicalDevice() const override;
        VkDevice getDevice() const override;

        const PhysicalDeviceInfo& getPhysicalDeviceInfo() const override;

        uint32_t requestQueue(const QueueRequest& request) override;

        void addRequiredInstanceLayer(const std::string& layer) override;
        void addOptionalInstanceLayer(const std::string& layer) override;

        void addRequiredInstanceExtension(const std::string& extension) override;
        void addOptionalInstanceExtension(const std::string& extension) override;
        
        void addRequiredDeviceExtension(const std::string& extension) override;
        void addOptionalDeviceExtension(const std::string& extension) override;

        const std::set<std::string>& getEnabledInstanceLayers() const override;
        const std::set<std::string>& getEnabledInstanceExtensions() const override;
        const std::set<std::string>& getEnabledDeviceExtensions() const override;

        const VkPhysicalDeviceFeatures& getEnabledDeviceFeatures() const override;

        VulkanQueue& getQueue(uint32_t id) override;

        void registerPreInstanceCreateHook(PreInstanceCreateHook hook) override;
        void registerPreDeviceSelectionHook(PreDeviceSelectionHook hook) override;
        void registerDeviceSelectionHook(DeviceSelectionHook hook) override;
        void registerDeviceSuitabilityHook(DeviceSuitabilityHook hook) override;
        void registerPostDeviceCreateHook(PostDeviceCreateHook hook) override;

        void _createInstance();
        void _selectDevice();
        void _createDevice();

        void _collectInstanceLayers(std::vector<const char*>& layers) const;
        void _collectInstanceExtensions(std::vector<const char*>& extensions) const;

        void _collectPhysicalDevices();
        void _preparePhysicalDevice(PhysicalDeviceMeta& device);
        bool _collectPhysicalDeviceExtensions(PhysicalDeviceMeta& device);
        void _generateQueueFamilyAssignments(PhysicalDeviceMeta& device);

        bool _stateGreaterEq(State min) const;
        bool _stateLessEq(State max) const;
        bool _stateInRange(State min, State max) const;

        void _dispatchPreInstanceCreate();
        void _dispatchPreDeviceSelection();
        void _dispatchDeviceSelection(PhysicalDeviceMeta& meta);
        bool _dispatchDeviceSuitability(PhysicalDeviceMeta& meta);
        void _dispatchPostDeviceCreate();

        RenderEngine& render_engine;
        Logger log;
        std::atomic<State> state{ State::UNINITIALIZED };

        VkInstance instance{ VK_NULL_HANDLE };
        std::set<std::string> enabled_instance_layers;
        std::set<std::string> enabled_instance_extensions;

        PhysicalDeviceInfo physical_device_info;

        VkDevice device{ VK_NULL_HANDLE };
        VkPhysicalDeviceFeatures enabled_device_features;
        std::set<std::string> enabled_device_extensions;

        std::vector<std::unique_ptr<VulkanQueue>> queues;
        std::vector<uint32_t> requested_queues; // Index of the associated queue in the queues vector for each request

        DisplayProvider* display_provider{ nullptr };

        std::unique_ptr<InitData> init_data;
    };
}