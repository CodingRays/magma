#include "vulkan_interface.hpp"

namespace magma::render_engine {

    VulkanQueue::VulkanQueue() {
    }

    VulkanQueue::VulkanQueue(VkQueue queue, uint32_t family) : queue{ queue }, family_index{ family } {
    }

    VulkanQueue::~VulkanQueue() {
    }

    std::mutex& VulkanQueue::getMutex() {
        return mutex;
    }

    VkQueue VulkanQueue::getQueue() {
        return queue;
    }

    uint32_t VulkanQueue::getQueueFamilyIndex() const {
        return family_index;
    }

    bool VulkanQueue::isValid() const {
        return queue != VK_NULL_HANDLE;
    }



    PhysicalDeviceInfo::PhysicalDeviceInfo() {
    }

    PhysicalDeviceInfo::PhysicalDeviceInfo(VkPhysicalDevice device) : device{ device } {
        vkGetPhysicalDeviceProperties(device, &properties);
        vkGetPhysicalDeviceFeatures(device, &supported_features);
        vkGetPhysicalDeviceMemoryProperties(device, &memory);

        uint32_t count;
        vkGetPhysicalDeviceQueueFamilyProperties(device, &count, nullptr);
        queues.resize(count);
        vkGetPhysicalDeviceQueueFamilyProperties(device, &count, queues.data());

        vkEnumerateDeviceExtensionProperties(device, nullptr, &count, nullptr);
        supported_extensions.resize(count);
        vkEnumerateDeviceExtensionProperties(device, nullptr, &count, supported_extensions.data());
    }


    
    PhysicalDeviceRequests::~PhysicalDeviceRequests() {
    }

    void PhysicalDeviceRequests::addRequiredExtension(const std::string& extension) {
        auto it{ optional_extensions.find(extension) };
        if(it != optional_extensions.end()) {
            optional_extensions.erase(it);
        }

        required_extensions.insert(extension);
    }

    void PhysicalDeviceRequests::addOptionalExtension(const std::string& extension) {
        auto it{ required_extensions.find(extension) };
        if(it == required_extensions.end()) {
            optional_extensions.insert(extension);
        }
    }

    void PhysicalDeviceRequests::addRequiredFeatures(const VkPhysicalDeviceFeatures& features) {
        throw std::runtime_error{ "Not implemented yet" }; // TODO
    }
    
    void PhysicalDeviceRequests::addOptionalFeatures(const VkPhysicalDeviceFeatures& features) {
        throw std::runtime_error{ "Not implemented yet" }; // TODO
    }

    void PhysicalDeviceRequests::registerSelectivePostDeviceCreateHook(PostDeviceCreateHook callback) {
        selective_post_device_create_hooks.emplace_back(callback);
    }

    const std::set<std::string>& PhysicalDeviceRequests::getRequiredExtensions() const {
        return required_extensions;
    }

    const std::set<std::string>& PhysicalDeviceRequests::getOptionalExtensions() const {
        return optional_extensions;
    }

    const VkPhysicalDeviceFeatures& PhysicalDeviceRequests::getRequiredFeatures() const {
        return required_features;
    }

    const VkPhysicalDeviceFeatures& PhysicalDeviceRequests::getOptionalFeatures() const {
        return optional_features;
    }

    void PhysicalDeviceRequests::dispatchSelectivePostDeviceCreateHooks(VulkanInterface& interface, Logger log) const {
        for(const PostDeviceCreateHook& cb : selective_post_device_create_hooks) {
            try {
                cb(interface);
            } catch(std::system_error& err) {
                log->error("System error during selective post device create hook: {}", err.what());
            } catch(std::runtime_error& err) {
                log->error("Runtime error during selective post device create hook: {}", err.what());
            } catch(...) {
                log->error("Unknown error during selective post device create hook");
            }
        }
    }
}