#include "render_engine_impl.hpp"

#include <cstring>

namespace magma::render_engine {

    RenderEngine_impl::PhysicalDeviceMeta::PhysicalDeviceMeta() {
    }

    RenderEngine_impl::PhysicalDeviceMeta::PhysicalDeviceMeta(VkPhysicalDevice device) : info{ device } {
    }

    RenderEngine_impl::RenderEngine_impl(RenderEngine& render_engine) : render_engine{ render_engine } {
        init_data = std::make_unique<InitData>();
        log = getOrCreateLogger("RenderEngine");
    }

    RenderEngine_impl::~RenderEngine_impl() {
    }

    void RenderEngine_impl::preInit() {
        log->debug("Pre init started");

        {
            State expected{ State::UNINITIALIZED };
            if(!state.compare_exchange_strong(expected, State::PRE_INIT_STARTED)) {
                log->error("Called preInit during invalid state: {}", expected);
                throw std::runtime_error{ "Called RenderEngine::preInit during invalid state" };
            }
        }
        
        state.store(State::PRE_INIT_COMPLETE);
        log->debug("Pre init complete");
    }

    void RenderEngine_impl::init() {
        log->debug("Init started");

        {
            State expected{ State::PRE_INIT_COMPLETE };
            if(!state.compare_exchange_strong(expected, State::INIT_STARTED)) {
                log->error("Called init during invalid state: {}", expected);
                throw std::runtime_error{ "Called RenderEngine::init during invalid state" };
            }
        }

        try {
            state.store(State::PRE_INSTANCE_CREATE);
            _dispatchPreInstanceCreate();

            state.store(State::INSTANCE_CREATE);
            _createInstance();

            state.store(State::PRE_DEVICE_SELECTION);
            _dispatchPreDeviceSelection();

            state.store(State::DEVICE_SELECTION);
            _selectDevice();

            state.store(State::DEVICE_CREATE);
            _createDevice();

            state.store(State::POST_DEVICE_CREATE);
            //init_data->selected_device->requests.dispatchSelectivePostDeviceCreateHooks(*this, log);
            _dispatchPostDeviceCreate();

        } catch(std::system_error& err) {
            log->critical("System error during initialization: {}", err.what());
            throw;
        } catch(std::runtime_error& err) {
            log->critical("Runtime error during initialization: {}", err.what());
            throw;
        } catch(...) {
            log->critical("Unknown error during initialization");
            throw;
        }

        state.store(State::INITIALIZED);
        log->debug("Init complete");
    }

    void RenderEngine_impl::destroy() {
        log->debug("Destroy started");

        State old_state{ state.load() };
        if(old_state == State::UNINITIALIZED) {
            return;
        }

        if(old_state == State::DESTROY_STARTED) {
            log->error("Called destroy while destroy is already running");
            return;
        }

        if(!state.compare_exchange_strong(old_state, State::DESTROY_STARTED)) {
            log->critical("Concurrent access to destroy");
            throw std::runtime_error{ "Concurrent access to destroy" };
        }

        // TODO
        if(device != VK_NULL_HANDLE) {
            vkDestroyDevice(device, nullptr);
            device = VK_NULL_HANDLE;
        }

        if(instance != VK_NULL_HANDLE) {
            vkDestroyInstance(instance, nullptr);
            instance = VK_NULL_HANDLE;
        }

        state.store(State::UNINITIALIZED);
        log->debug("Destroy complete");
    }

    VkInstance RenderEngine_impl::getInstance() const {
        if(_stateLessEq(State::INSTANCE_CREATE)) {
            log->warn("Tried to retrieve vulkan instance before instance is created");
        }

        return instance;
    }
    
    VkPhysicalDevice RenderEngine_impl::getPhysicalDevice() const {
        if(_stateLessEq(State::DEVICE_CREATE)) {
            log->warn("Tried to retrieve physical device before device is created");
        }

        return physical_device_info.device;
    }

    VkDevice RenderEngine_impl::getDevice() const {
        if(_stateLessEq(State::DEVICE_CREATE)) {
            log->warn("Tried to retrieve device before device is created");
        }

        return device;
    }

    const PhysicalDeviceInfo& RenderEngine_impl::getPhysicalDeviceInfo() const {
        if(_stateLessEq(State::DEVICE_CREATE)) {
            log->warn("Tried to retrieve physical device info before device is created");
        }

        return physical_device_info;
    }

    uint32_t RenderEngine_impl::requestQueue(const QueueRequest& request) {
        if(_stateGreaterEq(State::DEVICE_SELECTION)) {
            log->error("Tried to request queue after device selection has already begun. State: {}", state.load());
            throw std::runtime_error{ "Tried to request queue after device selection has already begun" };
        }

        uint32_t id{ static_cast<uint32_t>(init_data->requested_queues.size()) };
        init_data->requested_queues.emplace_back(request);
        return id;
    }

    void RenderEngine_impl::addRequiredInstanceLayer(const std::string& layer) {
        if(_stateGreaterEq(State::INSTANCE_CREATE)) {
            log->error("Tried to register required instance layer after instance create has already begun. State: {}", state.load());
            throw std::runtime_error{ "Tried to register required instance layer after instance create has already begun" };
        }

        auto it{ init_data->optional_instance_layers.find(layer) };
        if(it != init_data->optional_instance_layers.end()) {
            init_data->optional_instance_layers.erase(it);
        }

        init_data->required_instance_layers.emplace(layer);
    }

    void RenderEngine_impl::addOptionalInstanceLayer(const std::string& layer) {
        if(_stateGreaterEq(State::INSTANCE_CREATE)) {
            log->error("Tried to register optional instance layer after instance create has already begun. State: {}", state.load());
            throw std::runtime_error{ "Tried to register optional instance layer after instance create has already begun" };
        }
        
        auto it{ init_data->required_instance_layers.find(layer) };
        if(it == init_data->required_instance_layers.end()) {
            init_data->optional_instance_layers.emplace(layer);
        }
    }

    void RenderEngine_impl::addRequiredInstanceExtension(const std::string& extension) {
        if(_stateGreaterEq(State::INSTANCE_CREATE)) {
            log->error("Tried to register required instance extension after instance create has already begun. State: {}", state.load());
            throw std::runtime_error{ "Tried to register required instance extension after instance create has already begun" };
        }

        auto it{ init_data->optional_instance_extensions.find(extension) };
        if(it != init_data->optional_instance_extensions.end()) {
            init_data->optional_instance_extensions.erase(it);
        }

        init_data->required_instance_extensions.emplace(extension);
    }

    void RenderEngine_impl::addOptionalInstanceExtension(const std::string& extension) {
        if(_stateGreaterEq(State::INSTANCE_CREATE)) {
            log->error("Tried to register optional instance extension after instance create has already begun. State: {}", state.load());
            throw std::runtime_error{ "Tried to register optional instance extension after instance create has already begun" };
        }
        
        auto it{ init_data->required_instance_extensions.find(extension) };
        if(it == init_data->required_instance_extensions.end()) {
            init_data->optional_instance_extensions.emplace(extension);
        }
    }
        
    void RenderEngine_impl::addRequiredDeviceExtension(const std::string& extension) {
        if(_stateGreaterEq(State::DEVICE_SELECTION)) {
            log->error("Tried to register required device extension after device selection has already begun. State: {}", state.load());
            throw std::runtime_error{ "Tried to register required device extension after device selection has already begun" };
        }

        auto it{ init_data->optional_device_extensions.find(extension) };
        if(it != init_data->optional_device_extensions.end()) {
            init_data->optional_device_extensions.erase(it);
        }

        init_data->required_device_extensions.emplace(extension);
    }

    void RenderEngine_impl::addOptionalDeviceExtension(const std::string& extension) {
        if(_stateGreaterEq(State::DEVICE_SELECTION)) {
            log->error("Tried to register optional device extension after device selection has already begun. State: {}", state.load());
            throw std::runtime_error{ "Tried to optional required device extension after device selection has already begun" };
        }
        
        auto it{ init_data->required_device_extensions.find(extension) };
        if(it == init_data->required_device_extensions.end()) {
            init_data->optional_device_extensions.emplace(extension);
        }
    }

    const std::set<std::string>& RenderEngine_impl::getEnabledInstanceLayers() const {
        if(_stateLessEq(State::INSTANCE_CREATE)) {
            log->warn("Tried to retrieve enabled instance layers before instance is created");
        }

        return enabled_instance_layers;
    }

    const std::set<std::string>& RenderEngine_impl::getEnabledInstanceExtensions() const {
        if(_stateLessEq(State::INSTANCE_CREATE)) {
            log->warn("Tried to retrieve enabled instance extensions before instance is created");
        }

        return enabled_instance_extensions;
    }

    const std::set<std::string>& RenderEngine_impl::getEnabledDeviceExtensions() const {
        if(_stateLessEq(State::DEVICE_CREATE)) {
            log->warn("Tried to retrieve enabled device extensions before device is created");
        }

        return enabled_device_extensions;
    }

    const VkPhysicalDeviceFeatures& RenderEngine_impl::getEnabledDeviceFeatures() const {
        if(_stateLessEq(State::DEVICE_CREATE)) {
            log->warn("Tried to retrieve enabled device features before device is created");
        }

        return enabled_device_features;
    }

    VulkanQueue& RenderEngine_impl::getQueue(uint32_t id) {
        if(_stateLessEq(State::DEVICE_CREATE)) {
            log->error("Tried to retrieve queue before device is created");
            throw std::runtime_error{ "Tried to retrieve queue before device is created" };
        }

        return *queues[requested_queues.at(id)];
    }

    void RenderEngine_impl::registerPreInstanceCreateHook(PreInstanceCreateHook hook) {
        if(_stateGreaterEq(State::PRE_INSTANCE_CREATE)) {
            log->warn("Tried to register pre instance create hook after pre instance create has already begun. State: {}", state.load());
            return;
        }

        init_data->pre_instance_create_hooks.emplace_back(hook);
    }

    void RenderEngine_impl::registerPreDeviceSelectionHook(PreDeviceSelectionHook hook) {
        if(_stateGreaterEq(State::PRE_DEVICE_SELECTION)) {
            log->warn("Tried to register pre device selection hook after pre device selection has already begun. State: {}", state.load());
            return;
        }

        init_data->pre_device_selection_hooks.emplace_back(hook);
    }

    void RenderEngine_impl::registerDeviceSelectionHook(DeviceSelectionHook hook) {
        if(_stateGreaterEq(State::DEVICE_SELECTION)) {
            log->warn("Tried to register device selection hook after device selection has already begun. State: {}", state.load());
            return;
        }

        init_data->device_selection_hooks.emplace_back(hook);
    }

    void RenderEngine_impl::registerDeviceSuitabilityHook(DeviceSuitabilityHook hook) {
        if(_stateGreaterEq(State::DEVICE_SELECTION)) {
            log->warn("Tried to register device suitability hook after device selection has already begun. State: {}", state.load());
            return;
        }

        init_data->device_suitability_hooks.emplace_back(hook);
    }

    void RenderEngine_impl::registerPostDeviceCreateHook(PostDeviceCreateHook hook) {
        if(_stateGreaterEq(State::POST_DEVICE_CREATE)) {
            log->warn("Tried to register post device create hook after post device selection has alredy begun. State: {}", state.load());
            return;
        }

        init_data->post_device_create_hooks.emplace_back(hook);
    }

    void RenderEngine_impl::_createInstance() {
        std::vector<const char*> instance_layers;
        _collectInstanceLayers(instance_layers);
        std::vector<const char*> instance_extensions;
        _collectInstanceExtensions(instance_extensions);

        VkApplicationInfo app_info;
        app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        app_info.pNext = nullptr;
        app_info.pApplicationName = "Magma";
        app_info.applicationVersion = VK_MAKE_VERSION(MAGMA_VERSION_MAJOR, MAGMA_VERSION_MINOR, 0);
        app_info.pEngineName = "Magma";
        app_info.engineVersion = VK_MAKE_VERSION(MAGMA_VERSION_MAJOR, MAGMA_VERSION_MINOR, 0);
        app_info.apiVersion = VK_VERSION_1_1;

        VkInstanceCreateInfo info;
        info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        info.pNext = nullptr;
        info.flags = 0;
        info.pApplicationInfo = &app_info;
        info.enabledLayerCount = static_cast<uint32_t>(instance_layers.size());
        info.ppEnabledLayerNames = instance_layers.data();
        info.enabledExtensionCount = static_cast<uint32_t>(instance_extensions.size());
        info.ppEnabledExtensionNames = instance_extensions.data();

        VkResult res{ vkCreateInstance(&info, nullptr, &instance) };
        if(res != VK_SUCCESS) {
            log->critical("Failed to create vulkan instance {}", res);
            throw std::runtime_error{ "Failed to create vulkan instance" };
        }
    }

    void RenderEngine_impl::_selectDevice() {
        _collectPhysicalDevices();

        for(PhysicalDeviceMeta& meta : init_data->physical_devices) {
            log->debug("Found physical device: \"{}\"", meta.info.properties.deviceName);
            _dispatchDeviceSelection(meta);
        }

        for(PhysicalDeviceMeta& meta : init_data->physical_devices) {
            _preparePhysicalDevice(meta);
        }
    }

    void RenderEngine_impl::_createDevice() {
        PhysicalDeviceMeta& selected_device{ init_data->physical_devices[init_data->selected_device_index] };

        std::vector<const char*> layers(enabled_instance_layers.size());
        for(const std::string& layer : enabled_instance_layers) {
            layers.emplace_back(layer.c_str());
        }
        std::vector<const char*> extensions(selected_device.enabled_extensions.size());
        for(const std::string& extension : selected_device.enabled_extensions) {
            extensions.emplace_back(extension.c_str());
        }

        std::vector<VkDeviceQueueCreateInfo> queue_infos;
        queue_infos.reserve(selected_device.queue_metas.size());
        for(PhysicalDeviceMeta::QueueFamilyMeta& queue_meta : selected_device.queue_metas) {
            if(queue_meta.count > 0) {
                VkDeviceQueueCreateInfo& queue_info{ queue_infos.emplace_back() };
                queue_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
                queue_info.pNext = nullptr;
                queue_info.flags = 0;
                queue_info.queueFamilyIndex = queue_meta.family;
                queue_info.queueCount = queue_meta.count;
                queue_info.pQueuePriorities = queue_meta.priorities.data();
            }
        }

        VkDeviceCreateInfo info;
        info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        info.pNext = nullptr;
        info.flags = 0;
        info.queueCreateInfoCount = static_cast<uint32_t>(queue_infos.size());
        info.pQueueCreateInfos = queue_infos.data();
        info.enabledLayerCount = static_cast<uint32_t>(layers.size());
        info.ppEnabledLayerNames = layers.data();
        info.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
        info.ppEnabledExtensionNames = extensions.data();
        info.pEnabledFeatures = &selected_device.enabled_features;

        VkResult res{ vkCreateDevice(selected_device.info.device, &info, nullptr, &device) };
        if(res != VK_SUCCESS) {
            log->critical("Failed to create logical device {}", res);
            throw std::runtime_error{ "Failed to create logical device" };
        }

        queues.reserve(selected_device.queue_metas.size());
        for(auto& meta : selected_device.queue_metas) {
            VkQueue queue{ VK_NULL_HANDLE };

            vkGetDeviceQueue(device, meta.family, 0, &queue);

            queues.emplace_back(std::make_unique<VulkanQueue>(queue, meta.family));
        }

        requested_queues.resize(selected_device.queue_assignments.size());
        for(size_t i{ 0 }; i < selected_device.queue_assignments.size(); i++) {
            for(uint32_t q{ 0 }; q < queues.size(); q++) {
                if(queues[q]->getQueueFamilyIndex() == selected_device.queue_assignments[i].family_index) {
                    requested_queues[i] = q;
                    break;
                }
            }
        }
    }

    void RenderEngine_impl::_collectInstanceLayers(std::vector<const char*>& layers) const {
        uint32_t count;
        vkEnumerateInstanceLayerProperties(&count, nullptr);
        std::vector<VkLayerProperties> supported_layers(count);
        vkEnumerateInstanceLayerProperties(&count, supported_layers.data());

        layers.reserve(init_data->required_instance_layers.size() + init_data->optional_instance_layers.size());

        for(const std::string& layer : init_data->required_instance_layers) {
            bool found{ false };
            for(const VkLayerProperties& props : supported_layers) {
                if(strcmp(props.layerName, layer.c_str()) == 0) {
                    found = true;
                    break;
                }
            }

            if(!found) {
                log->critical("Missing required instance layer \"{}\"", layer);
                throw std::runtime_error{ "Missing required instance layer" };
            } else {
                log->debug("Enabling required instance layer \"{}\"", layer);
                layers.emplace_back(layer.c_str());
            }
        }

        for(const std::string& layer : init_data->optional_instance_layers) {
            bool found{ false };
            for(const VkLayerProperties& props : supported_layers) {
                if(strcmp(props.layerName, layer.c_str()) == 0) {
                    found = true;
                    break;
                }
            }

            if(found) {
                log->debug("Enabling optional instance layer \"{}\"", layer);
                layers.emplace_back(layer.c_str());
            }
        }
    }

    void RenderEngine_impl::_collectInstanceExtensions(std::vector<const char*>& extensions) const {
        uint32_t count;
        vkEnumerateInstanceExtensionProperties(nullptr, &count, nullptr);
        std::vector<VkExtensionProperties> supported_extensions(count);
        vkEnumerateInstanceExtensionProperties(nullptr, &count, supported_extensions.data());

        extensions.reserve(init_data->required_instance_extensions.size() + init_data->optional_instance_extensions.size());

        for(const std::string& extension : init_data->required_instance_extensions) {
            bool found{ false };
            for(const VkExtensionProperties& props : supported_extensions) {
                if(strcmp(props.extensionName, extension.c_str()) == 0) {
                    found = true;
                    break;
                }
            }

            if(!found) {
                log->critical("Missing required instance extension \"{}\"", extension);
                throw std::runtime_error{ "Missing required instance extension" };
            } else {
                log->debug("Enablind required instance extension \"{}\"", extension);
                extensions.emplace_back(extension.c_str());
            }
        }
                
        for(const std::string& extension : init_data->optional_instance_extensions) {
            bool found{ false };
            for(const VkExtensionProperties& props : supported_extensions) {
                if(strcmp(props.extensionName, extension.c_str()) == 0) {
                    found = true;
                    break;
                }
            }

            if(found) {
                log->debug("Enablind optional instance extension \"{}\"", extension);
                extensions.emplace_back(extension.c_str());
            }
        }
    }

    void RenderEngine_impl::_collectPhysicalDevices() {
        uint32_t count;
        vkEnumeratePhysicalDevices(instance, &count, nullptr);
        std::vector<VkPhysicalDevice> devices(count);
        vkEnumeratePhysicalDevices(instance, &count, devices.data());

        init_data->physical_devices.reserve(count);

        for(VkPhysicalDevice& device : devices) {
            PhysicalDeviceMeta& meta{ init_data->physical_devices.emplace_back(device) };
        }
    }

    void RenderEngine_impl::_preparePhysicalDevice(PhysicalDeviceMeta& device) {
        device.suitable = true;

        switch(device.info.properties.deviceType) {
            case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:
                device.rating = 50000;
                break;
            case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU:
                device.rating = 40000;
                break;
            case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:
                device.rating = 30000;
                break;
            default:
                device.rating = 10000;
        }

        if(!_dispatchDeviceSuitability(device)) {
            device.suitable = false;
        }

        if(!_collectPhysicalDeviceExtensions(device)) {
            device.suitable = false;
        }

        _generateQueueFamilyAssignments(device);
    }

    bool RenderEngine_impl::_collectPhysicalDeviceExtensions(PhysicalDeviceMeta& device) {
        for(const std::string& extension : init_data->required_device_extensions) {
            bool found{ false };
            for(VkExtensionProperties& props : device.info.supported_extensions) {
                if(strcmp(props.extensionName, extension.c_str()) == 0) {
                    found = true;
                    break;
                }
            }

            if(!found) {
                log->debug("Physical device \"{}\" missing global required extension \"{}\"", device.info.properties.deviceName, extension);
                return false;
            }

            device.enabled_extensions.emplace(extension);
        }

        for(const std::string& extension : device.requests.getRequiredExtensions()) {
            bool found{ false };
            for(VkExtensionProperties& props : device.info.supported_extensions) {
                if(strcmp(props.extensionName, extension.c_str()) == 0) {
                    found = true;
                    break;
                }
            }

            if(!found) {
                log->debug("Physical device \"{}\" missing special required extension \"{}\"", device.info.properties.deviceName, extension);
                return false;
            }

            device.enabled_extensions.emplace(extension);
        }
        
        for(const std::string& extension : init_data->optional_device_extensions) {
            bool found{ false };
            for(VkExtensionProperties& props : device.info.supported_extensions) {
                if(strcmp(props.extensionName, extension.c_str()) == 0) {
                    found = true;
                    break;
                }
            }

            if(found) {
                device.enabled_extensions.emplace(extension);
            }
        }

        for(const std::string& extension : device.requests.getOptionalExtensions()) {
            bool found{ false };
            for(VkExtensionProperties& props : device.info.supported_extensions) {
                if(strcmp(props.extensionName, extension.c_str()) == 0) {
                    found = true;
                    break;
                }
            }

            if(found) {
                device.enabled_extensions.emplace(extension);
            }
        }

        return true;
    }

    void RenderEngine_impl::_generateQueueFamilyAssignments(PhysicalDeviceMeta& device) {
        struct RequestMeta {
            uint32_t id;
            uint32_t family;
        };

        std::vector<RequestMeta> requests(init_data->requested_queues.size());
        for(uint32_t i{ 0 }; i < requests.size(); i++) {
            requests[i].id = i;
        }

        std::sort(requests.begin(), requests.end(), [&](const RequestMeta& a, const RequestMeta& b) -> bool{
            return init_data->requested_queues[a.id].priority > init_data->requested_queues[b.id].priority;
        });

        for(RequestMeta& meta : requests) {
            QueueRequest& request{ init_data->requested_queues[meta.id] };

            uint32_t match_count{ std::numeric_limits<uint32_t>::max() }; // The number of additional flags of the best family
            for(uint32_t i{ 0 }; i < device.info.queues.size(); i++) {
                VkQueueFamilyProperties& props{ device.info.queues[i] };

                if((request.flags & props.queueFlags) != request.flags) {
                    continue; // Not all flags found
                }

                if(request.queue_suitability_hook) {
                    if(!request.queue_suitability_hook(*this, device.info, i)) {
                        continue;
                    }
                }

                uint32_t remain_count{ 0 };
                VkQueueFlags remaining_flags{ props.queueFlags & (~props.queueFlags) };
                while(remaining_flags != 0) {
                    if(remaining_flags & 1) {
                        remain_count++;
                    }
                    remaining_flags = remaining_flags >> 1;
                }

                if(remain_count < match_count) {
                    meta.family = i;
                }
            }

            if(match_count == std::numeric_limits<uint32_t>::max()) {
                device.suitable = false; // No suitable queue family found
            }
        }

        device.queue_assignments.resize(requests.size());

        for(auto& i : requests) {
            device.queue_assignments[i.id].family_index = i.family;
            device.queue_assignments[i.id].sub_index = 0;
        }

        device.queue_metas.resize(device.info.queues.size());
        for(uint32_t i{ 0 }; i < device.info.queues.size(); i++) {
            device.queue_metas[i].count = 1;
            device.queue_metas[i].family = i;
            device.queue_metas[i].priorities.push_back(1.f);
        }
    }

    bool RenderEngine_impl::_stateGreaterEq(State min) const {
        return static_cast<uint32_t>(state.load()) >= static_cast<uint32_t>(min);
    }
    
    bool RenderEngine_impl::_stateLessEq(State max) const {
        return static_cast<uint32_t>(state.load()) <= static_cast<uint32_t>(max);
    }

    bool RenderEngine_impl::_stateInRange(State min, State max) const {
        uint32_t current = static_cast<uint32_t>(state.load());
        return (current >= static_cast<uint32_t>(min)) && (current <= static_cast<uint32_t>(max));
    }

    void RenderEngine_impl::_dispatchPreInstanceCreate() {
        log->trace("Dispatching pre instance create hooks ({})", init_data->pre_instance_create_hooks.size());
        for(PreInstanceCreateHook hook : init_data->pre_instance_create_hooks) {
            try {
                hook(*this);
            } catch(std::system_error& err) {
                log->error("System error while executing pre instance create hook: {}", err.what());
            } catch(std::runtime_error& err) {
                log->error("Runtime error while executing pre instance create hook: {}", err.what());
            } catch(...) {
                log->error("Unknown error while executing pre instance create hook");
            }
        }
    }

    void RenderEngine_impl::_dispatchPreDeviceSelection() {
        log->trace("Dispatching pre device selection hooks ({})", init_data->pre_device_selection_hooks.size());
        for(PreDeviceSelectionHook hook : init_data->pre_device_selection_hooks) {
            try {
                hook(*this);
            } catch(std::system_error& err) {
                log->error("System error while executing pre device selection hook: {}", err.what());
            } catch(std::runtime_error& err) {
                log->error("Runtime error while executing pre device selection hook: {}", err.what());
            } catch(...) {
                log->error("Unknown error while executing pre device selection hook");
            }
        }
    }

    void RenderEngine_impl::_dispatchDeviceSelection(PhysicalDeviceMeta& meta) {
        log->trace("Dispatching device selection hooks ({})", init_data->device_selection_hooks.size());
        for(DeviceSelectionHook hook : init_data->device_selection_hooks) {
            try {
                hook(*this, meta.info, meta.requests);
            } catch(std::system_error& err) {
                log->error("System error while executing device selection hook: {}", err.what());
            } catch(std::runtime_error& err) {
                log->error("Runtime error while executing device selection hook: {}", err.what());
            } catch(...) {
                log->error("Unknown error while executing device selection hook");
            }
        }
    }

    bool RenderEngine_impl::_dispatchDeviceSuitability(PhysicalDeviceMeta& meta) {
        log->trace("Dispatching device suitability hooks ({})", init_data->device_suitability_hooks.size());
        bool suitable{ true };
        for(DeviceSuitabilityHook hook : init_data->device_suitability_hooks) {
            try {
                if(!hook(*this, meta.info)) {
                    suitable = false;
                }
            } catch(std::system_error& err) {
                log->error("System error while executing device suitability hook: {}", err.what());
            } catch(std::runtime_error& err) {
                log->error("Runtime error while executing device suitability hook: {}", err.what());
            } catch(...) {
                log->error("Unknown error while executing device suitability hook");
            }
        }

        return suitable;
    }

    void RenderEngine_impl::_dispatchPostDeviceCreate() {
        log->trace("Dispatching post device create hoooks ({})", init_data->post_device_create_hooks.size());
        for(PostDeviceCreateHook hook : init_data->post_device_create_hooks) {
            try {
                hook(*this);
            } catch(std::system_error& err) {
                log->error("System error while executing post device create hook: {}", err.what());
            } catch(std::runtime_error& err) {
                log->error("Runtime error while executing post device create hook: {}", err.what());
            } catch(...) {
                log->error("Unknown error while executing post device create hook");
            }
        }
    }
}