#pragma once
#include "magma/include.hpp"
#include "include_vulkan.hpp"

#include <mutex>
#include <set>
#include <vector>

namespace magma::render_engine {

    class VulkanInterface;
    class PhysicalDeviceInfo;
    class PhysicalDeviceRequests;

    using PreInstanceCreateHook = std::function<void(VulkanInterface&)>;
    using PreDeviceSelectionHook = std::function<void(VulkanInterface&)>;
    using DeviceSelectionHook = std::function<void(VulkanInterface&, const PhysicalDeviceInfo&, PhysicalDeviceRequests&)>;
    using QueueSuitabilityHook = std::function<bool(VulkanInterface&, const PhysicalDeviceInfo&, uint32_t)>;
    using DeviceSuitabilityHook = std::function<bool(VulkanInterface&, const PhysicalDeviceInfo&)>;
    using PostDeviceCreateHook = std::function<void(VulkanInterface&)>;

    class VulkanQueue {
    public:
        VulkanQueue();
        VulkanQueue(VkQueue queue, uint32_t family);

        ~VulkanQueue();

        /**
         * Submits one or more commands to the queue.
         * 
         * Automatically manages synchronization.
         */
        VkResult queueSubmit(uint32_t submit_count, const VkSubmitInfo* submits, VkFence fence);

        /**
         * Returns the mutex used to synchronize access to the queue.
         */
        std::mutex& getMutex();

        /**
         * Returns the queue associated with this instance.
         * 
         * Any access to it must be synchronized using the mutex obtained
         * by calling getMutex().
         */
        VkQueue getQueue();

        uint32_t getQueueFamilyIndex() const;

        /**
         * Returns true if there is a queue asociated with this instance.
         */
        bool isValid() const;

    private:
        VkQueue queue{ VK_NULL_HANDLE };
        uint32_t family_index;

        std::mutex mutex;
    };

    struct QueueRequest {
        /**
         * The required queue flags.
         */
        VkQueueFlags flags{ 0 };

        /**
         * The required minimum image transfer granularity.
         * A value of { 0, 0, 0 } represents no limit.
         * 
         * Defaults to { 0, 0, 0 }.
         */ 
        VkExtent3D min_image_transfer_granularity{ 0, 0, 0 };

        /**
         * The priority of this queue. Is used to determine queue
         * assignments and priorities in vulkan.
         * 
         * Must be in range [0.f, 1.f].
         * 
         * Defaults to 0.5f;
         */
        float priority{ 0.5f };

        /**
         * If set called to test the suitablity of a certain queue family.
         */
        QueueSuitabilityHook queue_suitability_hook;
    };
    
    class PhysicalDeviceInfo {
    public:
        PhysicalDeviceInfo();
        PhysicalDeviceInfo(VkPhysicalDevice device);

        VkPhysicalDevice device{ VK_NULL_HANDLE };
        VkPhysicalDeviceProperties properties;
        VkPhysicalDeviceFeatures supported_features;
        VkPhysicalDeviceMemoryProperties memory;

        std::vector<VkExtensionProperties> supported_extensions;

        std::vector<VkQueueFamilyProperties> queues;
    };
    
    class PhysicalDeviceRequests {
    public:
        ~PhysicalDeviceRequests();

        void addRequiredExtension(const std::string& extension);
        void addOptionalExtension(const std::string& extension);

        void addRequiredFeatures(const VkPhysicalDeviceFeatures& features);
        void addOptionalFeatures(const VkPhysicalDeviceFeatures& features);

        /**
         * Hook is only called if this device is selected.
         * 
         * If a hook is registers both here and as a global post device create
         * hook it may be called multiple times.
         */
        void registerSelectivePostDeviceCreateHook(PostDeviceCreateHook hook);

        const std::set<std::string>& getRequiredExtensions() const;
        const std::set<std::string>& getOptionalExtensions() const;

        const VkPhysicalDeviceFeatures& getRequiredFeatures() const;
        const VkPhysicalDeviceFeatures& getOptionalFeatures() const;

        void dispatchSelectivePostDeviceCreateHooks(VulkanInterface& interface, Logger log) const;

    private:
        std::set<std::string> required_extensions;
        std::set<std::string> optional_extensions;
        
        VkPhysicalDeviceFeatures required_features{};
        VkPhysicalDeviceFeatures optional_features{};

        std::vector<PostDeviceCreateHook> selective_post_device_create_hooks;
    };

    class VulkanInterface {
    public:
        virtual VkInstance getInstance() const = 0;
        virtual VkPhysicalDevice getPhysicalDevice() const = 0;
        virtual VkDevice getDevice() const = 0;

        virtual const PhysicalDeviceInfo& getPhysicalDeviceInfo() const = 0;

        virtual uint32_t requestQueue(const QueueRequest& request) = 0;

        virtual void addRequiredInstanceLayer(const std::string& layer) = 0;
        virtual void addOptionalInstanceLayer(const std::string& layer) = 0;

        virtual void addRequiredInstanceExtension(const std::string& extension) = 0;
        virtual void addOptionalInstanceExtension(const std::string& extension) = 0;
        
        virtual void addRequiredDeviceExtension(const std::string& extension) = 0;
        virtual void addOptionalDeviceExtension(const std::string& extension) = 0;

        virtual const std::set<std::string>& getEnabledInstanceLayers() const = 0;
        virtual const std::set<std::string>& getEnabledInstanceExtensions() const = 0;
        virtual const std::set<std::string>& getEnabledDeviceExtensions() const = 0;

        virtual const VkPhysicalDeviceFeatures& getEnabledDeviceFeatures() const = 0;

        virtual VulkanQueue& getQueue(uint32_t id) = 0;

        virtual void registerPreInstanceCreateHook(PreInstanceCreateHook hook) = 0;
        virtual void registerPreDeviceSelectionHook(PreDeviceSelectionHook hook) = 0;
        virtual void registerDeviceSelectionHook(DeviceSelectionHook hook) = 0;
        virtual void registerDeviceSuitabilityHook(DeviceSuitabilityHook hook) = 0;
        virtual void registerPostDeviceCreateHook(PostDeviceCreateHook hook) = 0;
    };
}