#pragma once
#include "magma/include.hpp"

#include "include_vulkan.hpp"

namespace magma::render_engine {

    class DisplayProvider {
    public:
        /**
         * Displays one frame.
         * 
         * \param image The image data to display. Must not be VK_NULL_HANDLE.
         * \param wait_semaphore The semaphore to wait for before the image can be read. May be VK_NULL_HANDLE in which case the
         *                       image cane be used immediately.
         * \param signal_semaphores The semapores to signal after the image can be reused. Each entry must not be VK_NULL_HANDLE.
         * \param signal_fence The fence to signal after the image can be reused. May be VK_NULL_HANDLE.
         */
        virtual void displayFrame(VkImageView image, VkSemaphore wait_semaphore, const std::vector<VkSemaphore>& signal_semaphores, VkFence signal_fence) = 0;
    };
}