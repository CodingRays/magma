#pragma once
#include "magma/core_module.hpp"

namespace magma {
    namespace render_engine {
        class RenderEngine_impl;
        class VulkanInterface;
        class DisplayProvider;
    }

    class RenderEngine : public CoreModule {
    public:
        RenderEngine(Magma& engine);
        ~RenderEngine();

        void preInit() override;
        void init() override;

        void destroy() override;

        render_engine::VulkanInterface& getVulkan();

        void setDisplayProvider(render_engine::DisplayProvider* provider);

    private:
        std::unique_ptr<render_engine::RenderEngine_impl> instance;
    };
}