#include "magma.hpp"

#include "magma/detail/magma_impl.hpp"
#include <spdlog/sinks/stdout_color_sinks.h>

#include "transfer_engine.hpp"
#include <png.h>

#include <atomic>
#include <algorithm>
#include <array>
#include <cstring>
#include <chrono>
#include <set>

#include <iostream>

namespace magma {

    /*Logger vk_log;

    VKAPI_ATTR VkBool32 VKAPI_CALL _vkErrorCb(
        VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
        VkDebugUtilsMessageTypeFlagsEXT messageType,
        const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
        void* pUserData) {
        
        if(messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) {
            vk_log->warn("{0}", pCallbackData->pMessage);
        }
        else if(messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT) {
            vk_log->error("{0}", pCallbackData->pMessage);
        } else {
            vk_log->info("{0}", pCallbackData->pMessage);
        }

        return VK_TRUE;
    }

    Magma* magma_instance{ nullptr };

    void FramebufferResizeCb(GLFWwindow* window, int width, int height) {
        if(magma_instance) {
            magma_instance->onFramebufferResize();
        }
    }
    */
    Logger getOrCreateLogger(const std::string& name) {
        Logger log{ spdlog::get(name) };

        if(!log) {
            log = spdlog::stdout_color_mt(name);
        }

        log->set_level(spdlog::level::debug);
        return log;
    }/*

    Magma::~Magma() {
        _destroy();
    }

    uint32_t selectMemoryTypeIndex(VulkanBackend& backend) {
        VkInstance instance{ backend.getInstance() };
        VkDevice device{ backend.getDevice() };
        VkPhysicalDevice pdevice{ backend.getPhysicalDevice() };


        VkPhysicalDeviceMemoryProperties mem;
        vkGetPhysicalDeviceMemoryProperties(pdevice, &mem);

        for(uint32_t i{ 0 }; i < mem.memoryTypeCount; i++) {
            if(!(mem.memoryTypes[i].propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)) {
                continue;
            }

            if(!(mem.memoryTypes[i].propertyFlags & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)) {
                continue;
            }

            return i;
        }
        throw std::runtime_error{"HUH"};
    }

    void Magma::_testClearImage(Image* image) {
        VkCommandPoolCreateInfo pool_info;
        pool_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        pool_info.pNext = nullptr;
        pool_info.flags = 0;
        pool_info.queueFamilyIndex = backend.getQueueFamily(VulkanBackend::QueueId::RENDER);

        VkCommandPool pool;
        VkResult res{ vkCreateCommandPool(backend.getDevice(), &pool_info, nullptr, &pool) };
        if(res != VK_SUCCESS) {
            throw std::runtime_error{ "Failed to create command pool" };
        }

        VkCommandBufferAllocateInfo alloc_info;
        alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        alloc_info.pNext = nullptr;
        alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        alloc_info.commandPool = pool;
        alloc_info.commandBufferCount = 1;

        VkCommandBuffer cmd;
        res = vkAllocateCommandBuffers(backend.getDevice(), &alloc_info, &cmd);
        if(res != VK_SUCCESS) {
            throw std::runtime_error{ "Failed to allocate command buffer" };
        }

        VkCommandBufferBeginInfo begin_info;
        begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        begin_info.pNext = nullptr;
        begin_info.flags = 0;
        begin_info.pInheritanceInfo = 0;

        res = vkBeginCommandBuffer(cmd, &begin_info);
        if(res != VK_SUCCESS) {
            throw std::runtime_error{ "Failed to begin command buffer" };
        }

        VkImageSubresourceRange range;
        range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        range.baseMipLevel = 0;
        range.levelCount = VK_REMAINING_MIP_LEVELS;
        range.baseArrayLayer = 0;
        range.layerCount = VK_REMAINING_ARRAY_LAYERS;

        VkImageMemoryBarrier mem;
        mem.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        mem.pNext = nullptr;
        mem.srcAccessMask = 0;
        mem.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        mem.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        mem.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        mem.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        mem.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        mem.image = image->get();
        mem.subresourceRange = range;

        vkCmdPipelineBarrier(cmd, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &mem);

        VkClearColorValue value;
        value.float32[0] = 1.f;
        value.float32[1] = 0.f;
        value.float32[2] = 1.f;
        value.float32[3] = 1.f;

        vkCmdClearColorImage(cmd, image->get(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &value, 1, &range);

        mem.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        mem.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
        mem.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        mem.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

        vkCmdPipelineBarrier(cmd, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, 0, 0, nullptr, 0, nullptr, 1, &mem);

        res = vkEndCommandBuffer(cmd);
        if(res != VK_SUCCESS) {
            throw std::runtime_error{ "Failed to record command buffer" };
        }

        VkSubmitInfo submit_info;
        submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submit_info.pNext = nullptr;
        submit_info.commandBufferCount = 1;
        submit_info.pCommandBuffers = &cmd;
        submit_info.signalSemaphoreCount = 0;
        submit_info.pSignalSemaphores = nullptr;
        submit_info.waitSemaphoreCount = 0;
        submit_info.pWaitSemaphores = nullptr;
        submit_info.pWaitDstStageMask = nullptr;

        res = vkQueueSubmit(backend.getQueue(VulkanBackend::QueueId::RENDER), 1, &submit_info, VK_NULL_HANDLE);
        if(res != VK_SUCCESS) {
            log->error("Failed to submit clear to queue {}", res);
            throw std::runtime_error{ "Failed to submit clear to queue" };
        }
        vkQueueWaitIdle(backend.getQueue(VulkanBackend::QueueId::RENDER));

        vkDestroyCommandPool(backend.getDevice(), pool, nullptr);
    }

    void Magma::init() {
        log = getOrCreateLogger("Magma");
        if(!log) {
            throw std::runtime_error{ "Failed to create Magma logger" };
        }

        log->info("Initializing Magma");

        try {

            backend.init(log);

            image_registry.init(backend);
            shader_registry.init(backend);
            display.init(backend, shader_registry);

            GLFWwindow* window{ backend.getWindow() };
            glfwSetFramebufferSizeCallback(window, FramebufferResizeCb);
            glfwShowWindow(window);

            FILE* fp;
            errno_t err = fopen_s(&fp, "img.png", "rb");
            if(!fp) {
                log->error("Failed to open image file");
                throw std::runtime_error{"Failed to open image file"};
            }

            uint8_t buff[8];
            size_t s = fread(buff, 1, 8, fp);
            if(png_sig_cmp(buff, 0, 8)) {
                log->info("Sig bytes {}: 0x{:X} 0x{:X} 0x{:X} 0x{:X} 0x{:X} 0x{:X} 0x{:X} 0x{:X}", s, buff[0], buff[1], buff[2], buff[3], buff[4], buff[5], buff[6], buff[7]);
                log->error("File is not a PNG");
            }

            png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
            if(!png_ptr) {
                log->error("Failed to create png struct");
                throw std::runtime_error{"Failed to create png struct"};
            }
            png_infop info_ptr = png_create_info_struct(png_ptr);

            uint8_t* img_data{ nullptr };
            uint32_t img_width;
            uint32_t img_height;
            size_t img_size;
            if(!setjmp(png_jmpbuf(png_ptr))) {
                png_init_io(png_ptr, fp);
                png_set_sig_bytes(png_ptr, 8);

                png_read_info(png_ptr, info_ptr);

                img_width = png_get_image_width(png_ptr, info_ptr);
                img_height = png_get_image_height(png_ptr, info_ptr);

                log->info("Image size: {}x{}", img_width, img_height);

                size_t rowbytes{ png_get_rowbytes(png_ptr, info_ptr) };
                log->info("Data: {}", rowbytes);
                img_size = rowbytes * img_height;
                img_data = new uint8_t[img_size];
                for(size_t i{ 0 }; i< img_size; i++) {
                    img_data[i] = 0xAA;
                }
                png_bytep* row_ptrs = new png_bytep[img_height];
                for(uint32_t i{ 0 }; i < img_height; i++) {
                    row_ptrs[img_height - i - 1] = img_data + (rowbytes * i);
                }

                png_read_image(png_ptr, row_ptrs);

                delete[] row_ptrs;
            } else {
                log->error("Error while trying to read png file");
                throw std::runtime_error{"Error while trying to read png file"};
            }
            fclose(fp);

            Image* img{ image_registry.createImage({img_width, img_height, 1}, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT) };
            _testClearImage(img);

            ImageSubTransfer img_sub{};
            img_sub.data_offset = 0;
            img_sub.data_row_length = img_width;
            img_sub.data_image_height = img_height;

            img_sub.layers.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            img_sub.layers.baseArrayLayer = 0;
            img_sub.layers.layerCount = 1;
            img_sub.layers.mipLevel = 0;

            img_sub.image_offset = {0, 0, 0};
            img_sub.image_extent = {img_width, img_height, 1};

            ImageTransfer img_trans{ img_sub };
            img_trans.image = img->get();
            img_trans.dst_layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            img_trans.aspect_flags = VK_IMAGE_ASPECT_COLOR_BIT;

            TransferRequest trans{ img_trans, TransferRequest::ReadWrite::WRITE };
            trans.setQueueTransfer(VK_QUEUE_FAMILY_IGNORED, backend.getQueueFamily(VulkanBackend::QueueId::DISPLAY));
            trans.setWriteData(img_data, img_size);

            VkImageView view;
            VkImageViewCreateInfo info;
            info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = 0;
            info.image = img->get();
            info.viewType = VK_IMAGE_VIEW_TYPE_2D;
            info.format = VK_FORMAT_R8G8B8A8_UNORM;
            info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
            info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
            info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
            info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
            info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            info.subresourceRange.baseMipLevel = 0;
            info.subresourceRange.levelCount = 1;
            info.subresourceRange.baseArrayLayer = 0;
            info.subresourceRange.layerCount = 1;

            if(vkCreateImageView(backend.getDevice(), &info, nullptr, &view) != VK_SUCCESS) {
                throw std::runtime_error{ "Failed to create image view" };
            }

            TransferEngine engine;
            engine.init(backend);
            TransferPool pool{ engine, 10240000, selectMemoryTypeIndex(backend) };

            pool.transferSet({&trans});
            while(!pool.ready()) {
                std::this_thread::yield();
            }

            constexpr uint32_t frame_count{ 2048 };

            std::chrono::steady_clock::time_point t0{ std::chrono::steady_clock::now() };
            size_t i{ 0 };
            while(!glfwWindowShouldClose(window)) {
                glfwPollEvents();
                display.update();

                display.drawFrame(view, VK_NULL_HANDLE);

                i++;
                if(i == frame_count) {
                    std::chrono::steady_clock::time_point t1{ std::chrono::steady_clock::now() };

                    float dt{ static_cast<float>(std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count()) };
                    dt /= 1000000.f;

                    float fps = frame_count / dt;

                    log->info("FPS: {}", fps);
                    
                    i = 0;
                    t0 = std::chrono::steady_clock::now();
                }
            }

            vkDeviceWaitIdle(backend.getDevice());
            vkDestroyImageView(backend.getDevice(), view, nullptr);

        } catch(std::runtime_error& err) {
            log->error("Magma initialization failed: {}", err.what());
            _destroy();
            throw;
        } catch(...) {
            log->error("Magma initialization failed!");
            _destroy();
            throw;
        }
    }

    void Magma::destroy() {
        log->info("Destroying Magma");
        _destroy();
    }

    void Magma::onFramebufferResize() {
        display.enqueueRebuild();
    }

    void Magma::_destroy() {
        display.destroy();
        shader_registry.destroy();
        image_registry.destroy();
        backend.destroy();
    }*/

    std::atomic<RID> _next_rid{ 1 };

    RID createRID() {
        RID expected = _next_rid;
        while(!_next_rid.compare_exchange_weak(expected, expected + 1)) {
            expected += 1;
        }
        return expected;
    }

    Magma::Magma() {
    }

    Magma::~Magma() {
    }

    void Magma::init() {
        if(instance) {
            // TODO handle error
        }

        try {
            instance = std::make_unique<detail::Magma_impl>(*this);

            instance->init();
        } catch(...) {
            instance.release();
            throw;
        }
    }

    void Magma::destroy() {
        if(instance) {
            instance->destroy();
            instance.reset();
        }
    }

    RenderEngine& Magma::getRenderEngine() {
        return instance->renderer;
    }
}

int main() {
    magma::Magma instance;

    try {
        instance.init();
        instance.destroy();
    } catch(...) {
        spdlog::error("Engine crashed");
    }

    spdlog::info("Bye");

    return -1;
}