#include "image_registry.hpp"

#include "backend.hpp"

namespace magma {
    
    Image::Image() : id{ createRID() } {
    }

    Image::~Image() {
        if(image != VK_NULL_HANDLE) {
            Logger log{ getOrCreateLogger("ImageRegistry") };
            log->warn("Image destructor called before handle was destroyed. {}", id);
        }
    }

    void Image::init(VkImage img, VmaAllocation alloc) {
        image = img;
        allocation = alloc;
    }

    void Image::destroy(VmaAllocator allocator) {
        if(image != VK_NULL_HANDLE) {
            vmaDestroyImage(allocator, image, allocation);
            image = VK_NULL_HANDLE;
            allocation = nullptr;
        }
    }

    ImageRegistry::~ImageRegistry() {
        _destroy();
    }

    void ImageRegistry::init(VulkanBackend& backend) {
        log = getOrCreateLogger("ImageRegistry");
        if(!log) {
            throw std::runtime_error{ "Failed to get ImageRegistry logger" };
        }

        this->backend = &backend;
    }

    void ImageRegistry::destroy() {
        _destroy();
    }

    Image* ImageRegistry::createImage(VkExtent3D size, VkFormat format, VkImageUsageFlags usage) {
        VkImageType type{ VK_IMAGE_TYPE_3D };
        if(size.height <= 1 && size.depth <= 1) {
            type = VK_IMAGE_TYPE_1D;
        } else if(size.depth <= 1) {
            type = VK_IMAGE_TYPE_2D;
        }

        VkImageCreateInfo info;
        info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        info.pNext = nullptr;
        info.flags = 0;
        info.imageType = type;
        info.format = format;
        info.extent = size;
        info.mipLevels = 1;
        info.arrayLayers = 1;
        info.samples = VK_SAMPLE_COUNT_1_BIT;
        info.tiling = VK_IMAGE_TILING_OPTIMAL;
        info.usage = usage;
        info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        info.queueFamilyIndexCount = 0;
        info.pQueueFamilyIndices = nullptr;
        info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

        VmaAllocationCreateInfo alloc_info{};
        alloc_info.flags = 0;
        alloc_info.usage = VMA_MEMORY_USAGE_GPU_ONLY;
        alloc_info.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
        alloc_info.preferredFlags = 0;
        alloc_info.memoryTypeBits = 0;
        alloc_info.pool = nullptr;
        alloc_info.pUserData = nullptr;

        VkImage img_instance;
        VmaAllocation img_alloc{ nullptr };
        VkResult res{ vmaCreateImage(backend->getAllocator(), &info, &alloc_info, &img_instance, &img_alloc, nullptr) };
        if(res != VK_SUCCESS) {
            log->warn("Failed to create image {}", res);
            throw std::runtime_error{ "Failed to create image" };
        }

        Image* image{ _registerImage() };
        image->init(img_instance, img_alloc);

        return image;
    }

    void ImageRegistry::_destroy() {
        if(!images.empty()) {
            for(auto& img : images) {
                img.second->destroy(backend->getAllocator());
            }
            images.clear();
        }
    }

    Image* ImageRegistry::_registerImage() {
        std::unique_ptr<Image> image{ std::make_unique<Image>() };
        RID id{ image->getID() };
        Image* ret{ image.get() };

        images.emplace(id, std::move(image));

        return ret;
    }
}