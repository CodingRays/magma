#include "include.hpp"

#include <vk_mem_alloc.h>

#include <memory>
#include <map>

namespace magma {

    class VulkanBackend;

    class Image {
    public:
        Image();
        ~Image();

        void init(VkImage image, VmaAllocation allocation);
        void destroy(VmaAllocator allocator);

        inline RID getID() const;
        inline VkImage get() const;

    private:
        const RID id;

        VkImage image{ VK_NULL_HANDLE };
        VmaAllocation allocation{ nullptr };
    };

    class ImageRegistry {
    public:
        ~ImageRegistry();

        void init(VulkanBackend& backend);
        void destroy();

        Image* createImage(VkExtent3D size, VkFormat format, VkImageUsageFlags usage);

        void copyImageData(void* src, size_t src_size, Image* target);

    private:
        void _destroy();

        Image* _registerImage();

        VulkanBackend* backend{ nullptr };

        std::map<RID, std::unique_ptr<Image>> images;

        Logger log;
    };

    // Inline function definitions

    RID Image::getID() const {
        return id;
    }

    VkImage Image::get() const {
        return image;
    }
}