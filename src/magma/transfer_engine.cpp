#include "transfer_engine.hpp"

#include "backend.hpp"

#include <thread>

namespace magma {

    BufferTransfer::BufferTransfer() {
    }

    BufferTransfer::BufferTransfer(const BufferSubTransfer& transfer) : transfers{ transfer } {
    }

    BufferTransfer::BufferTransfer(VkBuffer buffer, VkDeviceSize size) : transfers{ BufferSubTransfer{ 0, 0, size }}, buffer{ buffer } {
    }

    ImageTransfer::ImageTransfer() {
    }

    ImageTransfer::ImageTransfer(const ImageSubTransfer& transfer) : transfers{ transfer } {
    }

    TransferRequest::TransferRequest(const BufferTransfer& trans, ReadWrite rw) : 
            type_rw{ static_cast<uint32_t>(Type::BUFFER) | static_cast<uint32_t>(rw) },
            buffer_transfer{ trans } {
    }

    TransferRequest::TransferRequest(const ImageTransfer& trans, ReadWrite rw) : 
            type_rw{ static_cast<uint32_t>(Type::IMAGE) | static_cast<uint32_t>(rw) },
            image_transfer{ trans } {
    }

    TransferRequest::TransferRequest(const TransferRequest& other) : 
            type_rw{ other.type_rw } {

        if(_getType() == Type::BUFFER) {
            new(&buffer_transfer) BufferTransfer(other.buffer_transfer);
        } else {
            new(&image_transfer) ImageTransfer(other.image_transfer);
        }
    }

    TransferRequest::~TransferRequest() {
        if(_getType() == Type::BUFFER) {
            buffer_transfer.~BufferTransfer();
        } else {
            image_transfer.~ImageTransfer();
        }
    }
    
    TransferRequest::ReadWrite TransferRequest::getReadWrite() const {
        return _getReadWrite();
    }

    bool TransferRequest::isRead() const {
        return _getReadWrite() == ReadWrite::READ;
    }

    bool TransferRequest::isWrite() const {
        return _getReadWrite() == ReadWrite::WRITE;
    }

    TransferRequest::Type TransferRequest::getTransferType() const {
        return _getType();
    }

    BufferTransfer& TransferRequest::getBufferInfo() {
        if(_getType() != Type::BUFFER) {
            throw std::runtime_error{ "Tried to access buffer data for image type transfer" };
        }
        return buffer_transfer;
    }

    ImageTransfer& TransferRequest::getImageInfo() {
        if(_getType() != Type::IMAGE) {
            throw std::runtime_error{ "Tried to access image data for buffer type transfer" };
        }
        return image_transfer;
    }

    void TransferRequest::setReadData(void* data, VkDeviceSize size) {
        if(_getReadWrite() != ReadWrite::READ) {
            throw std::runtime_error{ "Tried to set read data for write transfer" };
        }
        data_ptr = data;
        data_size = size;
    }
    
    void TransferRequest::setWriteData(const void* data, VkDeviceSize size) {
        if(_getReadWrite() != ReadWrite::WRITE) {
            throw std::runtime_error{ "Tried to set write data for read transfer" };
        }
        data_ptr = data;
        data_size = size;
    }

    void* TransferRequest::getReadData() const {
        if(_getReadWrite() != ReadWrite::READ) {
            throw std::runtime_error{ "Tried to access read data for write transfer" };
        }
        return const_cast<void*>(data_ptr);
    }

    const void* TransferRequest::getWriteData() const {
        if(_getReadWrite() != ReadWrite::WRITE) {
            throw std::runtime_error{ "Tried to access write data for read transfer" };
        }
        return data_ptr;
    }

    size_t TransferRequest::getDataSize() const {
        return data_size;
    }

    void TransferRequest::setQueueTransfer(uint32_t src, uint32_t dst) {
        src_queue_family = src;
        dst_queue_family = dst;
    }

    uint32_t TransferRequest::getSrcQueueFamily() const {
        return src_queue_family;
    }

    uint32_t TransferRequest::getDstQueueFamily() const {
        return dst_queue_family;
    }

    void TransferRequest::setAccessFlags(VkAccessFlags src, VkAccessFlags dst) {
        src_access = src;
        dst_access = dst;
    }

    VkAccessFlags TransferRequest::getSrcAccessFlags() const {
        return src_access;
    }

    VkAccessFlags TransferRequest::getDstAccessFlags() const {
        return dst_access;
    }

    void TransferRequest::configurePipelineStage(VkPipelineStageFlags src, VkPipelineStageFlags dst) {
        src_stage = src;
        dst_stage = dst;
    }

    VkPipelineStageFlags TransferRequest::getSrcPipelineStage() const {
        return src_stage;
    }

    VkPipelineStageFlags TransferRequest::getDstPipelineStage() const {
        return dst_stage;
    }
    
    TransferRequest::ReadWrite TransferRequest::_getReadWrite() const {
        return static_cast<ReadWrite>(type_rw & static_cast<uint32_t>(ReadWrite::READ_WRITE_MASK));
    }

    TransferRequest::Type TransferRequest::_getType() const {
        return static_cast<Type>(type_rw & static_cast<uint32_t>(Type::TYPE_MASK));
    }



    TransferPool::TransferPool(TransferEngine& engine, VkDeviceSize size, uint32_t index) :
            engine{ engine }, memory_size{ size }, memory_index{ index } {
        
        try {
            _init();

        } catch(std::runtime_error& error) {
            if(log) {
                log->error("Runtime error during initialization: {}", error.what());
            }
            _destroy();
            throw;

        } catch(...) {
            if(log) {
                log->error("Unknow error during initialization");
            }
            _destroy();
            throw;
        }
    }

    TransferPool::~TransferPool() {
        _destroy();
    }

    bool TransferPool::ready() const {
        return state == State::READY;
    }

    void TransferPool::transferSet(const std::vector<TransferRequest*>& transfers) {
        _waitReady();
        State expected{ State::READY };
        if(!state.compare_exchange_strong(expected, State::WRITING)) {
            log->error("Failed to update state. Is there a concurrent call?");
            throw std::runtime_error{ "Failed to update transfer pool state" };
        }

        current_transfers = transfers;

        _generateMemoryLayout();
        _recordCommandBuffer();
        _writeMemory();
        _submitCommandBuffer();
        
        _signalReadbackThread();
    }

    void TransferPool::transfer() {
        _waitReady();
        State expected{ State::READY };
        if(!state.compare_exchange_strong(expected, State::WRITING)) {
            log->error("Failed to update state. Is there a concurrent call?");
            throw std::runtime_error{ "Failed to update transfer pool state" };
        }

        _collectTransferRequests();
        _generateMemoryLayout();
        _recordCommandBuffer();
        _writeMemory();
        _submitCommandBuffer();
        
        _signalReadbackThread();
    }

    VkDeviceSize TransferPool::getMaxTransferSize() const {
        return memory_size;
    }

    void TransferPool::_init() {
        if(!log) {
            log = getOrCreateLogger("TransferPool");
            if(!log) {
                throw std::runtime_error{ "Failed to create transfer pool logger" };
            }
        }

        VkDevice device{ engine.getDevice() };

        VkBufferCreateInfo buffer_info;
        buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        buffer_info.pNext = nullptr;
        buffer_info.flags = 0;
        buffer_info.size = memory_size;
        buffer_info.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
        buffer_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        buffer_info.queueFamilyIndexCount = 0;
        buffer_info.pQueueFamilyIndices = nullptr;

        VkResult res{ vkCreateBuffer(device, &buffer_info, nullptr, &buffer) };
        if(res != VK_SUCCESS) {
            throw std::runtime_error{ "Failed to create transfer buffer" };
        }

        VkMemoryRequirements req;
        vkGetBufferMemoryRequirements(device, buffer, &req);

        if(!(req.memoryTypeBits & (1u << memory_index))) {
            log->error("Tried to create transfer pool with incompatible memory type. Bits: {:b}, Type: {}", req.memoryTypeBits, memory_index);
            throw std::runtime_error{ "Tried to create transfer pool with incompatible memory type" };
        }

        VkMemoryAllocateInfo alloc_info;
        alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        alloc_info.pNext = nullptr;
        alloc_info.allocationSize = req.size;
        alloc_info.memoryTypeIndex = memory_index;

        res = vkAllocateMemory(device, &alloc_info, nullptr, &memory);
        if(res != VK_SUCCESS) {
            throw std::runtime_error{ "Failed to allocate memory for transfer pool" };
        }

        res = vkBindBufferMemory(device, buffer, memory, 0);
        if(res != VK_SUCCESS) {
            throw std::runtime_error{ "Failed to bind transfer buffer to memory" };
        }

        VkFenceCreateInfo fence_info;
        fence_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        fence_info.pNext = nullptr;
        fence_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;

        res = vkCreateFence(device, &fence_info, nullptr, &cmd_completed_fence);
        if(res != VK_SUCCESS) {
            throw std::runtime_error{ "Failed to create fence for transfer pool" };
        }

        VkCommandBufferAllocateInfo cmd_info;
        cmd_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        cmd_info.pNext = nullptr;
        cmd_info.commandPool = engine.getCommandPool();
        cmd_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        cmd_info.commandBufferCount = 1;
        
        std::unique_lock<std::mutex> lock{ engine.getCommandMutex() };

        res = vkAllocateCommandBuffers(device, &cmd_info, &cmd);
        if(res != VK_SUCCESS) {
            throw std::runtime_error{ "Failed to allocate command buffers for transfer pool" };
        }

        readback_thread = std::thread{ &TransferPool::_runReadbackThread, this };
    }

    void TransferPool::_destroy() {
        readback_stop.store(true);
        readback_cv.notify_all();

        if(state != State::READY) {
            if(log) {
                log->warn("Destroying transfer pool that is currently in use. Waiting for command to complete.");
            }

            VkResult res{ vkGetFenceStatus(engine.getDevice(), cmd_completed_fence) };
            bool err{ res == VK_ERROR_DEVICE_LOST || res == VK_ERROR_OUT_OF_HOST_MEMORY || res == VK_ERROR_OUT_OF_DEVICE_MEMORY };
            if(res != VK_SUCCESS && !err) {
                res = vkWaitForFences(engine.getDevice(), 1, &cmd_completed_fence, VK_TRUE, 1000000000u);
            }
            if(res == VK_TIMEOUT) {
                log->critical("Timeout while waiting for fence");
            }
        }

        // After fence is signaled we can destroy most objects
        if(buffer != VK_NULL_HANDLE) {
            vkDestroyBuffer(engine.getDevice(), buffer, nullptr);
            buffer = VK_NULL_HANDLE;
        }

        if(memory != VK_NULL_HANDLE) {
            vkFreeMemory(engine.getDevice(), memory, nullptr);
            memory = VK_NULL_HANDLE;
        }

        if(cmd != VK_NULL_HANDLE) {
            std::unique_lock<std::mutex> lock{ engine.getCommandMutex(), std::defer_lock };
            try {
                lock.lock();

                vkFreeCommandBuffers(engine.getDevice(), engine.getCommandPool(), 1, &cmd);
                cmd = VK_NULL_HANDLE;
            } catch(...) {
                if(log) {
                    log->critical("Failed to lock command mutex while destroying a transfer pool");
                }
                // Cant rethrow since this may be inside a destructor
            }
        }

        if(readback_thread.joinable()) {
            try {
                readback_thread.join();
            } catch(std::system_error& err) {
                if(log) {
                    log->critical("Failed to join readback thread: {}", err.what());
                }
                // Cant rethrow since this may be inside a destructor
            }
        }

        // Thread is not using fence anymore
        if(cmd_completed_fence != VK_NULL_HANDLE) {
            vkDestroyFence(engine.getDevice(), cmd_completed_fence, nullptr);
            cmd_completed_fence = VK_NULL_HANDLE;
        }
    }

    void TransferPool::_waitReady(uint64_t timeout_ns) {
        while(state != State::READY) {
            std::this_thread::yield(); // TODO remove buisy waiting?
        }
    }

    void TransferPool::_collectTransferRequests() {
        throw std::runtime_error{ "Not implemented yet" };
    }

    void TransferPool::_generateMemoryLayout() {
        memory_layout.resize(current_transfers.size());

        VkDeviceSize offset{ 0 };
        for(size_t i{ 0 }; i < current_transfers.size(); i++) {
            memory_layout[i].offset = offset;

            offset += static_cast<VkDeviceSize>(current_transfers[i]->getDataSize());
            memory_layout[i].length = static_cast<VkDeviceSize>(current_transfers[i]->getDataSize());
        }

        if(offset > memory_size) {
            throw std::runtime_error{ "Not enough memory for transfer" };
        }
    }

    void TransferPool::_recordCommandBuffer() {
        std::vector<VkBufferMemoryBarrier> buffer_src_barriers;
        std::vector<VkImageMemoryBarrier> image_src_barrieres;
        std::vector<VkBufferMemoryBarrier> buffer_dst_barriers;
        std::vector<VkImageMemoryBarrier> image_dst_barrieres;

        for(TransferRequest* request : current_transfers) {
            if(request->getTransferType() == TransferRequest::Type::BUFFER) {
                _configureSrcBufferTransfer(
                    request,
                    buffer_src_barriers.emplace_back()
                );
                _configureDstBufferTransfer(
                    request,
                    buffer_dst_barriers.emplace_back()
                );

            } else {
                _configureSrcImageTransfer(
                    request,
                    image_src_barrieres.emplace_back()
                );
                _configureDstImageTransfer(
                    request,
                    image_dst_barrieres.emplace_back()
                );
            }
        }

        VkCommandBufferBeginInfo begin;
        begin.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        begin.pNext = nullptr;
        begin.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        begin.pInheritanceInfo = nullptr;

        std::unique_lock<std::mutex> lock{ engine.getCommandMutex() };

        VkResult res{ vkBeginCommandBuffer(cmd, &begin) };
        if(res != VK_SUCCESS) {
            throw std::runtime_error{ "Failed to begin command buffer" };
        }

        vkCmdPipelineBarrier(
            cmd, 
            VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            0, 
            0, nullptr,
            static_cast<uint32_t>(buffer_src_barriers.size()), buffer_src_barriers.data(),
            static_cast<uint32_t>(image_src_barrieres.size()), image_src_barrieres.data()
        );

        for(size_t i{ 0 }; i < current_transfers.size(); i++) {
            _recordCopyCmd(current_transfers[i], memory_layout[i]);
        }
        
        vkCmdPipelineBarrier(
            cmd, 
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
            0, 
            0, nullptr,
            static_cast<uint32_t>(buffer_dst_barriers.size()), buffer_dst_barriers.data(),
            static_cast<uint32_t>(image_dst_barrieres.size()), image_dst_barrieres.data()
        );

        res = vkEndCommandBuffer(cmd);

        lock.unlock();

        if(res != VK_SUCCESS) {
            throw std::runtime_error{ "Failed to end command buffer recording" };
        }
    }

    void TransferPool::_configureSrcBufferTransfer(TransferRequest* req, VkBufferMemoryBarrier& mem) {
        VkAccessFlags dst_access{ static_cast<VkAccessFlags>(req->isRead() ? VK_ACCESS_TRANSFER_READ_BIT : VK_ACCESS_TRANSFER_WRITE_BIT) };

        uint32_t dst_family{ engine.getQueueFamily() };
        if(req->getSrcQueueFamily() == VK_QUEUE_FAMILY_IGNORED) {
            dst_family = VK_QUEUE_FAMILY_IGNORED;
        }

        mem.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
        mem.pNext = nullptr;
        mem.srcAccessMask = req->getSrcAccessFlags();
        mem.dstAccessMask = dst_access;
        mem.srcQueueFamilyIndex = req->getSrcQueueFamily();
        mem.dstQueueFamilyIndex = dst_family;
        mem.buffer = req->getBufferInfo().buffer;
        mem.offset = 0;
        mem.size = VK_WHOLE_SIZE;
    }

    void TransferPool::_configureSrcImageTransfer(TransferRequest* req, VkImageMemoryBarrier& mem) {
        VkAccessFlags dst_access{ static_cast<VkAccessFlags>(req->isRead() ? VK_ACCESS_TRANSFER_READ_BIT : VK_ACCESS_TRANSFER_WRITE_BIT) };
        VkImageLayout dst_layout{ req->isRead() ? VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL : VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL };

        uint32_t dst_family{ engine.getQueueFamily() };
        if(req->getSrcQueueFamily() == VK_QUEUE_FAMILY_IGNORED) {
            dst_family = VK_QUEUE_FAMILY_IGNORED;
        }

        mem.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        mem.pNext = nullptr;
        mem.srcAccessMask = req->getSrcAccessFlags();
        mem.dstAccessMask = dst_access;
        mem.oldLayout = req->getImageInfo().src_layout;
        mem.newLayout = dst_layout;
        mem.srcQueueFamilyIndex = req->getSrcQueueFamily();
        mem.dstQueueFamilyIndex = dst_family;
        mem.image = req->getImageInfo().image;
        mem.subresourceRange.aspectMask = req->getImageInfo().aspect_flags;
        mem.subresourceRange.baseMipLevel = 0;
        mem.subresourceRange.levelCount = VK_REMAINING_MIP_LEVELS;
        mem.subresourceRange.baseArrayLayer = 0;
        mem.subresourceRange.layerCount = VK_REMAINING_ARRAY_LAYERS;
    }

    void TransferPool::_recordCopyCmd(TransferRequest* req, MemoryInfo& mem) {
        if(req->getTransferType() == TransferRequest::Type::BUFFER) {
            BufferTransfer& trans{ req->getBufferInfo() };
            
            std::vector<VkBufferCopy> copies(trans.transfers.size());
            const bool read{ req->isRead() };

            for(size_t i{ 0 }; i < trans.transfers.size(); i++) {
                VkBufferCopy& copy{ copies[i] };

                copy.size = trans.transfers[i].size;

                if(read) {
                    copy.srcOffset = trans.transfers[i].device_offset;   
                    copy.dstOffset = mem.offset + trans.transfers[i].host_offset; 
                } else {
                    copy.srcOffset = mem.offset + trans.transfers[i].host_offset;  
                    copy.dstOffset = trans.transfers[i].device_offset;
                }
            }

            VkBuffer src{ read ? trans.buffer : buffer };
            VkBuffer dst{ read ? buffer : trans.buffer };

            vkCmdCopyBuffer(cmd, src, dst, static_cast<uint32_t>(copies.size()), copies.data());

        } else {
            ImageTransfer& trans{ req->getImageInfo() };

            std::vector<VkBufferImageCopy> copies(trans.transfers.size());
            const bool read{ req->isRead() };

            for(size_t i{ 0 }; i < trans.transfers.size(); i++) {
                VkBufferImageCopy& copy{ copies[i] };

                copy.bufferOffset = mem.offset + trans.transfers[i].data_offset;
                copy.bufferRowLength = trans.transfers[i].data_row_length;
                copy.bufferImageHeight = trans.transfers[i].data_image_height;

                copy.imageSubresource = trans.transfers[i].layers;
                copy.imageOffset = trans.transfers[i].image_offset;
                copy.imageExtent = trans.transfers[i].image_extent;
            }

            if(read) {
                vkCmdCopyImageToBuffer(
                    cmd, 
                    trans.image, 
                    VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, 
                    buffer, 
                    static_cast<uint32_t>(copies.size()), copies.data()
                );
            } else {
                vkCmdCopyBufferToImage(
                    cmd, 
                    buffer, 
                    trans.image, 
                    VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 
                    static_cast<uint32_t>(copies.size()), copies.data()
                );
            }
        }
    }
    
    void TransferPool::_configureDstBufferTransfer(TransferRequest* req, VkBufferMemoryBarrier& mem) {
        VkAccessFlags src_access{ static_cast<VkAccessFlags>(req->isRead() ? VK_ACCESS_TRANSFER_READ_BIT : VK_ACCESS_TRANSFER_WRITE_BIT) };

        uint32_t src_family{ engine.getQueueFamily() };
        if(req->getDstQueueFamily() == VK_QUEUE_FAMILY_IGNORED) {
            src_family = VK_QUEUE_FAMILY_IGNORED;
        }

        mem.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
        mem.pNext = nullptr;
        mem.srcAccessMask = src_access;
        mem.dstAccessMask = req->getDstAccessFlags();
        mem.srcQueueFamilyIndex = src_family;
        mem.dstQueueFamilyIndex = req->getDstQueueFamily();
        mem.buffer = req->getBufferInfo().buffer;
        mem.offset = 0;
        mem.size = VK_WHOLE_SIZE;
    }

    void TransferPool::_configureDstImageTransfer(TransferRequest* req, VkImageMemoryBarrier& mem) {
        VkAccessFlags src_access{ static_cast<VkAccessFlags>(req->isRead() ? VK_ACCESS_TRANSFER_READ_BIT : VK_ACCESS_TRANSFER_WRITE_BIT) };
        VkImageLayout src_layout{ req->isRead() ? VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL : VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL };

        uint32_t src_family{ engine.getQueueFamily() };
        if(req->getDstQueueFamily() == VK_QUEUE_FAMILY_IGNORED) {
            src_family = VK_QUEUE_FAMILY_IGNORED;
        }

        mem.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        mem.pNext = nullptr;
        mem.srcAccessMask = src_access;
        mem.dstAccessMask = req->getDstAccessFlags();
        mem.oldLayout = src_layout;
        mem.newLayout = req->getImageInfo().dst_layout;
        mem.srcQueueFamilyIndex = src_family;
        mem.dstQueueFamilyIndex = req->getDstQueueFamily();
        mem.image = req->getImageInfo().image;
        mem.subresourceRange.aspectMask = req->getImageInfo().aspect_flags;
        mem.subresourceRange.baseMipLevel = 0;
        mem.subresourceRange.levelCount = VK_REMAINING_MIP_LEVELS;
        mem.subresourceRange.baseArrayLayer = 0;
        mem.subresourceRange.layerCount = VK_REMAINING_ARRAY_LAYERS;
    }

    void TransferPool::_submitCommandBuffer() {
        vkResetFences(engine.getDevice(), 1, &cmd_completed_fence);

        VkSubmitInfo info;
        info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        info.pNext = nullptr;
        info.waitSemaphoreCount = 0;
        info.pWaitSemaphores = nullptr;
        info.pWaitDstStageMask = nullptr;
        info.commandBufferCount = 1;
        info.pCommandBuffers = &cmd;
        info.signalSemaphoreCount = 0;
        info.pSignalSemaphores = nullptr;

        std::unique_lock<std::mutex> lock{ engine.getCommandMutex() };

        VkResult res{ vkQueueSubmit(engine.getQueue(), 1, &info, cmd_completed_fence) };

        lock.unlock();

        if(res != VK_SUCCESS) {
            state.store(State::READY); // Prevents infinite wait during destruction TODO Better way to handle error
            throw std::runtime_error{ "Failed to submit transfer command to queue" };
        }

        State expected{ State::WRITING };
        if(!state.compare_exchange_strong(expected, State::COPYING)) {
            log->error("Failed to update state after command buffer submit");
            throw std::runtime_error{ "Failed to update state after command buffer submit" };
        }
    }

    void TransferPool::_writeMemory() {
        void* data_ptr;
        
        VkResult res{ vkMapMemory(engine.getDevice(), memory, 0, VK_WHOLE_SIZE, 0, &data_ptr) };
        if(res != VK_SUCCESS) {
            throw std::runtime_error{ "Failed to map memory for transfer write" };
        }

        try {
            for(size_t i{ 0 }; i < current_transfers.size(); i++) {
                TransferRequest* req{ current_transfers[i] };
                if(req->isWrite()) {
                    const void* src{ req->getWriteData() };
                    const size_t src_size{ req->getDataSize() };

                    void* dst{ reinterpret_cast<uint8_t*>(data_ptr) + memory_layout[i].offset };
                    const size_t dst_size{ memory_size - memory_layout[i].offset };
                    memcpy_s(dst, dst_size, src, src_size);
                }
            }

            vkUnmapMemory(engine.getDevice(), memory);
        } catch(...) {
            vkUnmapMemory(engine.getDevice(), memory);
            throw;
        }
    }

    void TransferPool::_readMemory() {
        void* data_ptr;
        
        VkResult res{ vkMapMemory(engine.getDevice(), memory, 0, VK_WHOLE_SIZE, 0, &data_ptr) };
        if(res != VK_SUCCESS) {
            log->critical("Failed to map memory for transfer read {}", res);
            throw std::runtime_error{ "Failed to map memory for transfer read" };
        }

        try {
            for(size_t i{ 0 }; i < current_transfers.size(); i++) {
                TransferRequest* req{ current_transfers[i] };
                if(req->isRead()) {
                    const void* src{ reinterpret_cast<uint8_t*>(data_ptr) + memory_layout[i].offset };

                    void* dst{ req->getReadData() };
                    const size_t dst_size{ req->getDataSize() };

                    memcpy_s(dst, dst_size, src, dst_size);
                }
            }

            vkUnmapMemory(engine.getDevice(), memory);
        } catch(...) {
            log->error("Error during read operation");
            vkUnmapMemory(engine.getDevice(), memory);
            throw;
        }
    }

    void TransferPool::_signalReadbackThread() {
        readback_cv.notify_all();
    }

    void TransferPool::_runReadbackThread() {
        log->debug("Starting readback thread");
        do {
            log->debug("Readback state: Waiting for transfer");
            
            do {
                std::unique_lock lock{ readback_mutex };
                readback_cv.wait_for(lock, std::chrono::milliseconds(1));
                
                if(readback_stop.load()) {
                    return;
                }
            } while(state.load() != State::COPYING);

            log->debug("Readback state: Waiting for transfer complete");

            VkResult res;
            do {
                res = vkWaitForFences(engine.getDevice(), 1, &cmd_completed_fence, VK_TRUE, 1000000u);

                if(readback_stop.load()) {
                    return;
                }

                if(res != VK_SUCCESS && res != VK_TIMEOUT) {
                    log->warn("Error while waiting for fence in readback thread");
                    return;
                }
            } while(res == VK_TIMEOUT);

            log->debug("Readback state: Reading data");

            State expected{ State::COPYING };
            if(!state.compare_exchange_strong(expected, State::READING)) {
                log->warn("Failed to set state in readback thread");
                return;
            }

            _readMemory();

            expected = State::READING;
            if(!state.compare_exchange_strong(expected, State::READY)) {
                log->warn("Failed to set state in readback thread");
                return;
            }
        } while(!readback_stop.load());
    }

    Logger TransferPool::log;

    TransferEngine::TransferEngine() {
    }

    TransferEngine::~TransferEngine() {
        _destroy();
    }

    void TransferEngine::init(VulkanBackend& backend) {
        this->backend = &backend;

        cmd_queue = backend.getQueue(VulkanBackend::QueueId::TRANSFER);

        VkCommandPoolCreateInfo info;
        info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        info.pNext = VK_NULL_HANDLE;
        info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
        info.queueFamilyIndex = backend.getQueueFamily(VulkanBackend::QueueId::TRANSFER);

        VkResult res{ vkCreateCommandPool(backend.getDevice(), &info, nullptr, &cmd_pool) };
        if(res != VK_SUCCESS) {
            throw std::runtime_error{ "Failed to create command pool for transfer engine" };
        }
    }

    void TransferEngine::destroy() {
        _destroy();
    }

    std::mutex& TransferEngine::getCommandMutex() {
        return cmd_mutex;
    }

    VkDevice TransferEngine::getDevice() {
        return backend->getDevice();
    }

    VkQueue TransferEngine::getQueue() {
        return cmd_queue;
    }

    uint32_t TransferEngine::getQueueFamily() {
        return backend->getQueueFamily(VulkanBackend::QueueId::TRANSFER);
    }

    VkCommandPool TransferEngine::getCommandPool() {
        return cmd_pool;
    }

    void TransferEngine::_destroy() {
        if(cmd_pool) {
            vkDestroyCommandPool(backend->getDevice(), cmd_pool, nullptr);
            cmd_pool = VK_NULL_HANDLE;
        }
    }
}