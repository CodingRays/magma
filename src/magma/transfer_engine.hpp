#include "include.hpp"

#include <condition_variable>
#include <mutex>
#include <thread>
#include <vector>

namespace magma {
    
    struct BufferSubTransfer {
        VkDeviceSize host_offset{ 0 };
        VkDeviceSize device_offset{ 0 };
        VkDeviceSize size{ 0 };
    };

    struct BufferTransfer {
        BufferTransfer();
        BufferTransfer(const BufferSubTransfer& transfer);

        BufferTransfer(const BufferTransfer&) = default;
        BufferTransfer(BufferTransfer&&) = default;

        BufferTransfer& operator=(const BufferTransfer&) = default;
        BufferTransfer& operator=(BufferTransfer&&) = default;

        /**
         * Utility constructor that copies copy_size amounts of data.
         * Both host and device offset are set to 0.
         */
        BufferTransfer(VkBuffer buffer, VkDeviceSize copy_size);

        VkBuffer buffer{ VK_NULL_HANDLE };
        std::vector<BufferSubTransfer> transfers;
    };

    struct ImageSubTransfer {
        VkDeviceSize data_offset{ 0 };
        uint32_t data_row_length{ 0 };
        uint32_t data_image_height{ 0 };

        VkImageSubresourceLayers layers;
        VkOffset3D image_offset;
        VkExtent3D image_extent;
    };

    struct ImageTransfer {
        ImageTransfer();
        ImageTransfer(const ImageSubTransfer& transfer);

        ImageTransfer(const ImageTransfer&) = default;
        ImageTransfer(ImageTransfer&&) = default;
        
        ImageTransfer& operator=(const ImageTransfer&) = default;
        ImageTransfer& operator=(ImageTransfer&&) = default;

        VkImage image{ VK_NULL_HANDLE };
        VkImageLayout src_layout{ VK_IMAGE_LAYOUT_UNDEFINED };
        VkImageLayout dst_layout{ VK_IMAGE_LAYOUT_UNDEFINED };
        VkImageAspectFlags aspect_flags;
        std::vector<ImageSubTransfer> transfers;
    };

    class TransferRequest {
    public:
        enum class ReadWrite : uint32_t {
            READ = 0x1,
            WRITE = 0x2,
            READ_WRITE_MASK = 0xFFFF,
        };

        enum class Type : uint32_t {
            BUFFER = 0x10000,
            IMAGE = 0x20000,
            TYPE_MASK = 0xFFFF0000,
        };

        TransferRequest(const BufferTransfer& transfer, ReadWrite rw);
        TransferRequest(const ImageTransfer& transfer, ReadWrite rw);

        TransferRequest(const TransferRequest& other);

        ~TransferRequest();

        ReadWrite getReadWrite() const;

        bool isRead() const;
        bool isWrite() const;

        Type getTransferType() const;

        BufferTransfer& getBufferInfo();
        ImageTransfer& getImageInfo();

        void setReadData(void* data, VkDeviceSize size);
        void setWriteData(const void* data, VkDeviceSize size);

        void* getReadData() const;
        const void* getWriteData() const;

        size_t getDataSize() const;

        /**
         * Configures queue family transfer.
         * 
         * If src or dst are VK_QUEUE_FAMILY_IGNORED then ownership transfer
         * is skipped for that barrier operation.
         * By default both src and dst are set to VK_QUEUE_FAMILY_IGNORED.
         * 
         * \param src The queue family that currently owns the resource.
         * \param dst The queue family that should own the resource after
         *            the transfer is complete.
         */
        void setQueueTransfer(uint32_t src, uint32_t dst);

        uint32_t getSrcQueueFamily() const;
        uint32_t getDstQueueFamily() const;

        /**
         * Configure memory access flags.
         * 
         * By default both src and dst are (VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT).
         * 
         * \param src The access flags for synchronization before the transfer.
         * \param dst The access flags for synchronization after the transfer.
         */
        void setAccessFlags(VkAccessFlags src, VkAccessFlags dst);

        VkAccessFlags getSrcAccessFlags() const;
        VkAccessFlags getDstAccessFlags() const;

        /**
         * Configure pipeline stage mask for execution barrier.
         * 
         * By default src is VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT and dst is
         * VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT.
         * 
         * \param src The wait stage for synchronization before the transfer.
         * \param dst The wait stage for synchronization after the transfer.
         */
        void configurePipelineStage(VkPipelineStageFlags src, VkPipelineStageFlags dst);

        VkPipelineStageFlags getSrcPipelineStage() const;
        VkPipelineStageFlags getDstPipelineStage() const;

    private:
        ReadWrite _getReadWrite() const;
        Type _getType() const;

        const void* data_ptr{ nullptr };
        VkDeviceSize data_size{ 0 };

        uint32_t src_queue_family{ VK_QUEUE_FAMILY_IGNORED };
        uint32_t dst_queue_family{ VK_QUEUE_FAMILY_IGNORED };

        VkAccessFlags src_access{ VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT };
        VkAccessFlags dst_access{ VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT };

        VkPipelineStageFlags src_stage{ VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT };
        VkPipelineStageFlags dst_stage{ VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT };

        // Combined type and read write
        const uint32_t type_rw;
        union {
            BufferTransfer buffer_transfer;
            ImageTransfer image_transfer;
        };
    };

    class TransferEngine;

    class TransferPool {
    public:
        enum class State {
            READY,
            WRITING,
            COPYING,
            READING,
        };

        TransferPool(TransferEngine& engine, VkDeviceSize size, uint32_t memory_index);
        ~TransferPool();

        /**
         * Returns true if this pool is ready to transfer data.
         * E.g. There are no ongoing transfers or errors.
         */
        bool ready() const;

        /**
         * Starts a transfer using a predefined set of objects.
         * 
         * If the transfers are too big to fit into a single transfer throws an runtime_error.
         * 
         * If there is a ongoing transfer blocks until it is complete.
         */
        void transferSet(const std::vector<TransferRequest*>& transfers);

        /**
         * Causes the pool to collect suitable transfers from the TransferEngine and 
         * starts a transfer.
         * 
         * If there is a ongoing transfer blocks until it is complete.
         */
        void transfer();

        /**
         * Returns the maximum size in bytes this pool can transfer in one transfer.
         */
        VkDeviceSize getMaxTransferSize() const;

    private:
        struct MemoryInfo {
            VkDeviceSize offset;
            VkDeviceSize length;
        };

        void _init();
        void _destroy();

        void _waitReady(uint64_t timeout_ns = 10000000000u);

        void _collectTransferRequests();
        void _generateMemoryLayout();
        
        void _recordCommandBuffer();
        void _configureSrcBufferTransfer(TransferRequest* req, VkBufferMemoryBarrier& mem);
        void _configureSrcImageTransfer(TransferRequest* req, VkImageMemoryBarrier& mem);
        void _recordCopyCmd(TransferRequest* req, MemoryInfo& info);
        void _configureDstBufferTransfer(TransferRequest* req, VkBufferMemoryBarrier& mem);
        void _configureDstImageTransfer(TransferRequest* req, VkImageMemoryBarrier& mem);

        void _submitCommandBuffer();

        void _writeMemory();
        void _readMemory();

        void _signalReadbackThread();
        void _runReadbackThread();

        static Logger log;

        TransferEngine& engine;

        VkDeviceMemory memory{ VK_NULL_HANDLE };
        VkBuffer buffer{ VK_NULL_HANDLE };
        const VkDeviceSize memory_size;
        const uint32_t memory_index;

        VkFence cmd_completed_fence{ VK_NULL_HANDLE };
        VkCommandBuffer cmd{ VK_NULL_HANDLE };

        std::vector<TransferRequest*> current_transfers;
        std::vector<MemoryInfo> memory_layout;

        std::atomic<State> state{ State::READY };

        std::thread readback_thread;
        std::atomic<bool> readback_stop{ false };
        std::mutex readback_mutex;
        std::condition_variable readback_cv;
    };

    class VulkanBackend;

    class TransferEngine {
    public:
        TransferEngine();
        ~TransferEngine();

        void init(VulkanBackend& backend);
        void destroy();

        std::mutex& getCommandMutex();
        VkDevice getDevice();
        VkQueue getQueue();
        uint32_t getQueueFamily();
        VkCommandPool getCommandPool();

    private:
        void _destroy();

        VulkanBackend* backend{ nullptr };

        std::mutex cmd_mutex;
        VkQueue cmd_queue{ VK_NULL_HANDLE };
        VkCommandPool cmd_pool{ VK_NULL_HANDLE };
    };
}