#include "display.hpp"

#include "backend.hpp"
#include "shader_registry.hpp"

#include <algorithm>
#include <fstream>
#include <limits>

namespace magma {
    
    void loadShaderCode(std::vector<char>& buffer, const std::string& path) {
        std::ifstream in{ path, std::ios::ate | std::ios::binary };

        if(!in) {
            throw std::runtime_error{ "Failed to open file" };
        }

        buffer.resize(static_cast<size_t>(in.tellg()));
        in.seekg(0);
        in.read(buffer.data(), buffer.size());

        in.close();
    }

    DisplayEngine::~DisplayEngine() {
        _destroy();
    }

    void DisplayEngine::init(VulkanBackend& backend, ShaderRegistry& registry) {
        {
            State expected{ State::UNINITIALIZED };
            if(!state.compare_exchange_strong(expected, State::INIT_STARTED)) {
                log->error("Tried to call DisplayEngne::init when instance is not in uninitialized state");
                throw std::runtime_error{ "Tried to call DisplayEngne::init when instance is not in uninitialized state" };
            }
        }

        try {
        log = getOrCreateLogger("MagmaDisplay");
            if(!log) {
                throw std::runtime_error{ "Failed to create display engien logger" };
            }
        
            this->backend = &backend;
            this->registry = &registry;
            display_queue = backend.getQueue(VulkanBackend::QueueId::DISPLAY);

            registry.registerShaderModule("display", "shaders/display.spv");

            _createDescriptorSetLayout();
            _createSampler();
            _createPipelineLayout();
            _createSurface();
            _createSwapchain();
            _createRenderPass();
            _createPipeline();
            _createFramebuffers();
            _updatePerImageSyncObjects();
            _createDescriptorSets();

            VkFenceCreateInfo fence;
            fence.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
            fence.pNext = nullptr;
            fence.flags = VK_FENCE_CREATE_SIGNALED_BIT;

            vkCreateFence(backend.getDevice(), &fence, nullptr, &draw_done_fence);

            VkSemaphoreCreateInfo sem;
            sem.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
            sem.pNext = nullptr;
            sem.flags = 0;

            image_ready_semaphores.resize(2);
            vkCreateSemaphore(backend.getDevice(), &sem, nullptr, &image_ready_semaphores[0]);
            vkCreateSemaphore(backend.getDevice(), &sem, nullptr, &image_ready_semaphores[1]);

            _createCommandPool();
            _updateCommandBuffers();
        } catch(...) {
            state.store(State::FAILED);
            throw;
        }

        State expected{ State::INIT_STARTED };
        state.compare_exchange_strong(expected, State::INITIALIZED);
    }

    void DisplayEngine::destroy() {
        _destroy();
    }

    void DisplayEngine::update() {
        if(rebuild_needed) {
            _rebuildSwapchain();
            rebuild_needed = false;
        }
    }

    void DisplayEngine::drawFrame(VkImageView src, VkSemaphore wait_sem) {
        uint32_t iteration{ draw_iteration++ };

        VkSemaphore image_ready = image_ready_semaphores[iteration % image_ready_semaphores.size()];

        uint32_t next_image;
        VkResult res{ vkAcquireNextImageKHR(backend->getDevice(), swapchain, std::numeric_limits<uint64_t>::max(), image_ready, VK_NULL_HANDLE, &next_image) };
        if(res == VK_ERROR_OUT_OF_DATE_KHR) {
            enqueueRebuild();
            return;
        }
        else if(res == VK_TIMEOUT || res == VK_NOT_READY) {
            return;
        }
        else if(res == VK_SUBOPTIMAL_KHR) {
            enqueueRebuild();
        }
        else if(res != VK_SUCCESS) {
            log->error("Failed to acquire image {}", res);
            throw std::runtime_error{"Failed to acquire image"};
        }

        VkDescriptorSet descriptor_set{ descriptor_sets[next_image] };

        VkDescriptorImageInfo image_info;
        image_info.sampler = sampler;
        image_info.imageView = src;
        image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

        VkWriteDescriptorSet desc_write;
        desc_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        desc_write.pNext = nullptr;
        desc_write.dstSet = descriptor_set;
        desc_write.dstBinding = 0;
        desc_write.dstArrayElement = 0;
        desc_write.descriptorCount = 1;
        desc_write.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        desc_write.pImageInfo = &image_info;
        desc_write.pBufferInfo = nullptr;
        desc_write.pTexelBufferView = nullptr;

        vkUpdateDescriptorSets(backend->getDevice(), 1, &desc_write, 0, nullptr);

        VkCommandBuffer cmd{ cmd_buffers[next_image] };

        VkCommandBufferBeginInfo begin_info;
        begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        begin_info.pNext = nullptr;
        begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        begin_info.pInheritanceInfo = nullptr;

        res = vkBeginCommandBuffer(cmd, &begin_info);
        if(res != VK_SUCCESS) {
            log->error("Failed to begin command buffer recording {}", res);
            throw std::runtime_error{ "Failed to begin command buffer recording" };
        }

        VkRenderPassBeginInfo render_info;
        render_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        render_info.pNext = nullptr;
        render_info.renderPass = render_pass;
        render_info.framebuffer = framebuffers[next_image];
        render_info.renderArea.offset = { 0, 0 };
        render_info.renderArea.extent = display_size;
        render_info.clearValueCount = 0;
        render_info.pClearValues = nullptr;

        vkCmdBeginRenderPass(cmd, &render_info, VK_SUBPASS_CONTENTS_INLINE);
        vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
        vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, layout, 0, 1, &descriptor_set, 0, nullptr);
        vkCmdDraw(cmd, 4, 1, 0, 0);
        vkCmdEndRenderPass(cmd);

        res = vkEndCommandBuffer(cmd);
        if(res != VK_SUCCESS) {
            log->error("Failed to record command buffer");
            throw std::runtime_error{ "Failed to record command buffer" };
        }

        VkPipelineStageFlags wait_stages[]{ VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT };
        VkSemaphore wait_semaphores[]{ image_ready, wait_sem };

        VkSemaphore draw_done{ draw_done_semaphores[next_image] };

        VkSubmitInfo submit_info;
        submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submit_info.pNext = nullptr;
        submit_info.waitSemaphoreCount = wait_sem != VK_NULL_HANDLE ? 2 : 1;
        submit_info.pWaitSemaphores = wait_semaphores;
        submit_info.pWaitDstStageMask = wait_stages;
        submit_info.commandBufferCount = 1;
        submit_info.pCommandBuffers = &cmd;
        submit_info.signalSemaphoreCount = 1;
        submit_info.pSignalSemaphores = &draw_done;

        res = vkWaitForFences(backend->getDevice(), 1, &draw_done_fence, VK_TRUE, 100000000u);
        if(res == VK_TIMEOUT) {
            log->warn("Wait for fence timed out");
            throw std::runtime_error{ "Wait for fence timed out" };
        }
        if(res != VK_SUCCESS) {
            log->error("Failed to wait for fence");
            throw std::runtime_error{ "Failed to wait for fence" };
        }

        res = vkResetFences(backend->getDevice(), 1, &draw_done_fence);
        if(res != VK_SUCCESS) {
            log->error("Failed to reset fence");
            throw std::runtime_error{ "Failed to reset fence" };
        }

        res = vkQueueSubmit(display_queue, 1, &submit_info, draw_done_fence);
        if(res != VK_SUCCESS) {
            log->error("Failed to submit queue {}", res);
            throw std::runtime_error{ "Failed to submit queue" };
        }

        VkPresentInfoKHR present_info;
        present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        present_info.pNext = nullptr;
        present_info.waitSemaphoreCount = 1;
        present_info.pWaitSemaphores = &draw_done;
        present_info.swapchainCount = 1;
        present_info.pSwapchains = &swapchain;
        present_info.pImageIndices = &next_image;
        present_info.pResults = &res;

        VkResult nres{ vkQueuePresentKHR(display_queue, &present_info) };
        if(nres == VK_ERROR_OUT_OF_DATE_KHR) {
            enqueueRebuild();
            return;
        }
        if(nres != VK_SUCCESS && nres != VK_SUBOPTIMAL_KHR) {
            log->error("Failed to present to surface {}", nres);
            throw std::runtime_error{ "Failed to present to surface" };
        }
    }

    void DisplayEngine::enqueueRebuild() {
        rebuild_needed = true;
    }

    VkExtent2D DisplayEngine::getDisplaySize() const {
        return display_size;
    }

    void DisplayEngine::_destroy() {
        State expected{ state.load() };
        if(expected != State::INITIALIZED && expected != State::FAILED) {
            return;
        }

        if(!state.compare_exchange_strong(expected, State::DESTROY_STARTED)) {
            return;
        }

        log->debug("Destroying DisplayEngine");
        vkQueueWaitIdle(backend->getQueue(VulkanBackend::QueueId::DISPLAY));

        cmd_buffers.clear();

        if(cmd_pool) {
            vkDestroyCommandPool(backend->getDevice(), cmd_pool, nullptr);
            cmd_pool = VK_NULL_HANDLE;
        }

        if(!draw_done_semaphores.empty()) {
            for(VkSemaphore sem : draw_done_semaphores) {
                if(sem != VK_NULL_HANDLE) {
                    vkDestroySemaphore(backend->getDevice(), sem, nullptr);
                }
            }
            draw_done_semaphores.clear();
        }

        if(!image_ready_semaphores.empty()) {
            for(VkSemaphore sem : image_ready_semaphores) {
                if(sem) {
                    vkDestroySemaphore(backend->getDevice(), sem, nullptr);
                }
            }
            image_ready_semaphores.clear();
        }

        if(draw_done_fence) {
            vkDestroyFence(backend->getDevice(), draw_done_fence, nullptr);
            draw_done_fence = VK_NULL_HANDLE;
        }

        descriptor_sets.clear();
        if(descriptor_pool) {
            vkDestroyDescriptorPool(backend->getDevice(), descriptor_pool, nullptr);
            descriptor_pool = VK_NULL_HANDLE;
        }

        if(!framebuffers.empty()) {
            for(VkFramebuffer framebuffer : framebuffers) {
                if(framebuffer != VK_NULL_HANDLE) {
                    vkDestroyFramebuffer(backend->getDevice(), framebuffer, nullptr);
                }
            }
            framebuffers.clear();
        }

        if(pipeline) {
            vkDestroyPipeline(backend->getDevice(), pipeline, nullptr);
            pipeline = VK_NULL_HANDLE;
        }

        if(render_pass) {
            vkDestroyRenderPass(backend->getDevice(), render_pass, nullptr);
            render_pass = VK_NULL_HANDLE;
        }

        if(!swapchain_views.empty()) {
            for(VkImageView view : swapchain_views) {
                if(view) {                
                    vkDestroyImageView(backend->getDevice(), view, nullptr);
                }
            }
            swapchain_views.clear();
        }

        swapchain_images.clear();

        if(swapchain) {
            vkDestroySwapchainKHR(backend->getDevice(), swapchain, nullptr);
            swapchain = VK_NULL_HANDLE;
        }

        if(surface) {
            vkDestroySurfaceKHR(backend->getInstance(), surface, nullptr);
            surface = VK_NULL_HANDLE;
        }

        if(layout) {
            vkDestroyPipelineLayout(backend->getDevice(), layout, nullptr);
            layout = VK_NULL_HANDLE;
        }

        if(set_layout) {
            vkDestroyDescriptorSetLayout(backend->getDevice(), set_layout, nullptr);
            set_layout = VK_NULL_HANDLE;
        }

        if(sampler) {
            vkDestroySampler(backend->getDevice(), sampler, nullptr);
            sampler = VK_NULL_HANDLE;
        }

        display_queue = VK_NULL_HANDLE;
        backend = nullptr;

        state.store(State::UNINITIALIZED);
    }

    void DisplayEngine::_rebuildSwapchain() {
        log->info("Rebuilding swapchain");

        vkQueueWaitIdle(display_queue);

        _prepareSwapchainRecreate();

        _createSwapchain();
        _createRenderPass();
        _createPipeline();
        _createFramebuffers();
        _createDescriptorSets();
        _updatePerImageSyncObjects();
        _updateCommandBuffers();

        log->info("Rebuild complete. New size ({}, {})", display_size.width, display_size.height);
    }

    void DisplayEngine::_createDescriptorSetLayout() {
        VkDescriptorSetLayoutBinding binding;
        binding.binding = 0;
        binding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        binding.descriptorCount = 1;
        binding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
        binding.pImmutableSamplers = nullptr;

        VkDescriptorSetLayoutCreateInfo info;
        info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        info.pNext = nullptr;
        info.flags = 0;
        info.bindingCount = 1;
        info.pBindings = &binding;

        VkResult res{ vkCreateDescriptorSetLayout(backend->getDevice(), &info, nullptr, &set_layout) };
        if(res != VK_SUCCESS) {
            log->error("Failed to create desciptor set layout {}", res);
            throw std::runtime_error{ "Failed to crate desciptor set layout" };
        }
    }

    void DisplayEngine::_createSampler() {
        VkSamplerCreateInfo info;
        info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
        info.pNext = nullptr;
        info.flags = 0;
        info.magFilter = VK_FILTER_LINEAR;
        info.minFilter = VK_FILTER_LINEAR;
        info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
        info.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        info.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        info.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        info.mipLodBias = 0.f;
        info.anisotropyEnable = VK_FALSE;
        info.maxAnisotropy = 0.f;
        info.compareEnable = VK_FALSE;
        info.compareOp = VK_COMPARE_OP_NEVER;
        info.minLod = 0.f;
        info.maxLod = 0.f;
        info.borderColor = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK;
        info.unnormalizedCoordinates = VK_FALSE;

        VkResult res{ vkCreateSampler(backend->getDevice(), &info, nullptr, &sampler) };
        if(res != VK_SUCCESS) {
            log->error("Failed to create sampler {}", res);
            throw std::runtime_error{ "Failed to create sampler" };
        }
    }

    void DisplayEngine::_createPipelineLayout() {
        VkPipelineLayoutCreateInfo info;
        info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        info.pNext = nullptr;
        info.flags = 0;
        info.setLayoutCount = 1;
        info.pSetLayouts = &set_layout;
        info.pushConstantRangeCount = 0;
        info.pPushConstantRanges = nullptr;
    
        VkResult res{ vkCreatePipelineLayout(backend->getDevice(), &info, nullptr, &layout) };
        if(res != VK_SUCCESS) {
            log->error("Failed to create pipeline layout {}", res);
            throw std::runtime_error{ "Failed to create pipeline layout" };
        }
    }

    void DisplayEngine::_createCommandPool() {
        VkCommandPoolCreateInfo info;
        info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        info.pNext = nullptr;
        info.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT | VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
        info.queueFamilyIndex = backend->getQueueFamily(VulkanBackend::QueueId::DISPLAY);

        VkResult res{ vkCreateCommandPool(backend->getDevice(), &info, nullptr, &cmd_pool) };
        if(res != VK_SUCCESS) {
            log->error("Failed to create command pool {}", res);
            throw std::runtime_error{ "Failed to create command pool" };
        }
    }

    void DisplayEngine::_createSurface() {
        VkResult res{ glfwCreateWindowSurface(backend->getInstance(), backend->getWindow(), nullptr, &surface) };
        if(res != VK_SUCCESS) {
            log->error("Failed to create window surface {}", res);
            throw std::runtime_error{ "Failed to create window surface" };
        }

        VkBool32 supported;
        res = vkGetPhysicalDeviceSurfaceSupportKHR(backend->getPhysicalDevice(), backend->getQueueFamily(VulkanBackend::QueueId::DISPLAY), surface, &supported);
        if(res != VK_SUCCESS || !supported) {
            log->error("Device does not support display to surface");
            throw std::runtime_error{ "Device does not support display to surface" };
        }
    }

    VkSurfaceFormatKHR DisplayEngine::_selectSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& formats) const {
        bool comp4_found{ false };
        VkSurfaceFormatKHR comp4;
        for(size_t i{ 0 }; i < formats.size(); i++) {
            if(formats[i].colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
                if(formats[i].format == VK_FORMAT_R8G8B8_UNORM) {
                    return formats[i];
                }

                if(formats[i].format == VK_FORMAT_B8G8R8_UNORM) {
                    return formats[i];
                }

                if(formats[i].format == VK_FORMAT_R8G8B8A8_UNORM) {
                    comp4 = formats[i];
                    comp4_found = true;
                }

                if(formats[i].format == VK_FORMAT_B8G8R8A8_UNORM) {
                    comp4 = formats[i];
                    comp4_found = true;
                }
            }
        }

        if(comp4_found) {
            return comp4;
        }

        return formats[0];
    }

    VkPresentModeKHR DisplayEngine::_selectPresentMode(const std::vector<VkPresentModeKHR>& modes) const {
        for(size_t i{ 0 }; i < modes.size(); i++) {
            if(modes[i] == VK_PRESENT_MODE_MAILBOX_KHR) {
                return modes[i];
            }
        }

        return VK_PRESENT_MODE_FIFO_KHR;
    }

    VkExtent2D DisplayEngine::_selectSwapExtent(const VkSurfaceCapabilitiesKHR& caps) const {
        if(caps.currentExtent.width != UINT32_MAX) {
            return caps.currentExtent;
        }

        int w, h;
        glfwGetFramebufferSize(const_cast<GLFWwindow*>(backend->getWindow()), &w, &h);
        VkExtent2D extent{ static_cast<uint32_t>(w), static_cast<uint32_t>(h) };

        extent.width = std::clamp(extent.width, caps.minImageExtent.width, caps.maxImageExtent.width);
        extent.height = std::clamp(extent.height, caps.minImageExtent.height, caps.maxImageExtent.height);

        return extent;
    }

    void DisplayEngine::_createSwapchain() {
        if(swapchain) {
            for(VkImageView view : swapchain_views) {
                vkDestroyImageView(backend->getDevice(), view, nullptr);
            }
        }

        VkSurfaceCapabilitiesKHR capabilities;
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(backend->getPhysicalDevice(), surface, &capabilities);

        if(!(capabilities.supportedCompositeAlpha & VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR)) {
            log->error("Opaque composite alpha not supported");
            throw std::runtime_error{ "Opaque composite alpha not supported" };
        }
        
        if(!(capabilities.supportedUsageFlags & VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT)) {
            log->error("Color attachement usage for swapchain not supported");
            throw std::runtime_error{ "Color attachement usage for swapchain not supported" };
        }

        display_size = _selectSwapExtent(capabilities);
        uint32_t image_count{ std::clamp(3u, capabilities.minImageCount, capabilities.maxImageCount) };

        {
            uint32_t count;
            vkGetPhysicalDeviceSurfaceFormatsKHR(backend->getPhysicalDevice(), surface, &count, nullptr);
            std::vector<VkSurfaceFormatKHR> formats( count );
            vkGetPhysicalDeviceSurfaceFormatsKHR(backend->getPhysicalDevice(), surface, &count, formats.data());
            surface_format = _selectSurfaceFormat(formats);
        }

        VkPresentModeKHR mode;
        {
            uint32_t count;
            vkGetPhysicalDeviceSurfacePresentModesKHR(backend->getPhysicalDevice(), surface, &count, nullptr);
            std::vector<VkPresentModeKHR> modes( count );
            vkGetPhysicalDeviceSurfacePresentModesKHR(backend->getPhysicalDevice(), surface, &count, modes.data());
            mode = _selectPresentMode(modes);
        }


        VkSwapchainKHR old_swapchain{ swapchain };

        VkSwapchainCreateInfoKHR info;
        info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        info.pNext = nullptr;
        info.flags = 0;
        info.surface = surface;
        info.minImageCount = image_count;
        info.imageFormat = surface_format.format;
        info.imageColorSpace = surface_format.colorSpace;
        info.imageExtent = display_size;
        info.imageArrayLayers = 1;
        info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
        info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        info.queueFamilyIndexCount = 0;
        info.pQueueFamilyIndices = nullptr;
        info.preTransform = capabilities.currentTransform;
        info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        info.presentMode = mode;
        info.clipped = VK_TRUE;
        info.oldSwapchain = old_swapchain;
        
        VkResult res{ vkCreateSwapchainKHR(backend->getDevice(), &info, nullptr, &swapchain) };

        if(old_swapchain != VK_NULL_HANDLE) {
            vkDestroySwapchainKHR(backend->getDevice(), old_swapchain, nullptr);
        }

        if(res != VK_SUCCESS) {
            log->error("Failed to create swapchain {}", res);
            throw std::runtime_error{ "Failed to create swapchain" };
        }

        vkGetSwapchainImagesKHR(backend->getDevice(), swapchain, &swapchain_image_count, nullptr);
        swapchain_images.resize(swapchain_image_count);
        vkGetSwapchainImagesKHR(backend->getDevice(), swapchain, &swapchain_image_count, swapchain_images.data());

        swapchain_views.resize(swapchain_image_count);
        for(VkImageView &view : swapchain_views) {
            view = VK_NULL_HANDLE; // In case of error know which to destroy
        }

        for(uint32_t i{ 0 }; i < swapchain_image_count; i++) {
            VkImageViewCreateInfo view_info;
            view_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
            view_info.pNext = nullptr;
            view_info.flags = 0;
            view_info.image = swapchain_images[i];
            view_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
            view_info.format = surface_format.format;
            view_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
            view_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
            view_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
            view_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
            view_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            view_info.subresourceRange.baseMipLevel = 0;
            view_info.subresourceRange.levelCount = 1;
            view_info.subresourceRange.baseArrayLayer = 0;
            view_info.subresourceRange.layerCount = 1;

            VkResult res{ vkCreateImageView(backend->getDevice(), &view_info, nullptr, &(swapchain_views[i])) };
            if(res != VK_SUCCESS) {
                log->error("Failed to create image view {}", res);
                throw std::runtime_error{ "Failed to create image view" };
            }
        }
    }
    
    void DisplayEngine::_createRenderPass() {
        VkAttachmentDescription attachment;
        attachment.flags = 0;
        attachment.format = surface_format.format;
        attachment.samples = VK_SAMPLE_COUNT_1_BIT;
        attachment.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        VkAttachmentReference out_reference;
        out_reference.attachment = 0;
        out_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        VkSubpassDescription subpass;
        subpass.flags = 0;
        subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass.inputAttachmentCount = 0;
        subpass.pInputAttachments = nullptr;
        subpass.colorAttachmentCount = 1;
        subpass.pColorAttachments = &out_reference;
        subpass.pResolveAttachments = nullptr;
        subpass.pDepthStencilAttachment = nullptr;
        subpass.preserveAttachmentCount = 0;
        subpass.pPreserveAttachments = nullptr;

        VkSubpassDependency dependency;
        dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
        dependency.dstSubpass = 0;
        dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.srcAccessMask = 0;
        dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        dependency.dependencyFlags = 0;

        VkRenderPassCreateInfo info;
        info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        info.pNext = nullptr;
        info.flags = 0;
        info.attachmentCount = 1;
        info.pAttachments = &attachment;
        info.subpassCount = 1;
        info.pSubpasses = &subpass;
        info.dependencyCount = 1;
        info.pDependencies = &dependency;

        VkResult res{ vkCreateRenderPass(backend->getDevice(), &info, nullptr, &render_pass) };
        if(res != VK_SUCCESS) {
            log->error("Failed to create render pass {}", res);
            throw std::runtime_error{ "Failed to create render pass" };
        }
    }

    void DisplayEngine::_createPipeline() {
        VkPipelineShaderStageCreateInfo shader_stages[2];

        const ShaderEntryPoint* vertex_shader{ registry->findEntryPoint("display", "vertex") };
        const ShaderEntryPoint* fragment_shader{ registry->findEntryPoint("display", "fragment") };
        if(!vertex_shader) {
            log->error("Unable to find vertex shader");
            throw std::runtime_error{ "Unable to find display vertex shader" };
        }
        if(!fragment_shader) {
            log->error("Unable to find fragment shader");
            throw std::runtime_error{ "Unable to find display fragment shader" };
        }

        vertex_shader->configureShaderStageInfo(shader_stages[0]);
        fragment_shader->configureShaderStageInfo(shader_stages[1]);

        VkPipelineVertexInputStateCreateInfo vertex_state;
        vertex_state.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        vertex_state.pNext = nullptr;
        vertex_state.flags = 0;
        vertex_state.vertexBindingDescriptionCount = 0;
        vertex_state.pVertexBindingDescriptions = nullptr;
        vertex_state.vertexAttributeDescriptionCount = 0;
        vertex_state.pVertexAttributeDescriptions = nullptr;

        VkPipelineInputAssemblyStateCreateInfo input_state;
        input_state.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        input_state.pNext = nullptr;
        input_state.flags = 0;
        input_state.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN;
        input_state.primitiveRestartEnable = VK_FALSE;

        VkViewport viewport;
        viewport.x = 0.f;
        viewport.y = 0.f;
        viewport.width = static_cast<float>(display_size.width);
        viewport.height = static_cast<float>(display_size.height);
        viewport.minDepth = 0.f;
        viewport.maxDepth = 1.f;

        VkRect2D scissor;
        scissor.offset = {0, 0};
        scissor.extent = display_size;

        VkPipelineViewportStateCreateInfo viewport_state;
        viewport_state.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        viewport_state.pNext = nullptr;
        viewport_state.flags = 0;
        viewport_state.viewportCount = 1;
        viewport_state.pViewports = &viewport;
        viewport_state.scissorCount = 1;
        viewport_state.pScissors = &scissor;
        
        VkPipelineRasterizationStateCreateInfo rasterization_state;
        rasterization_state.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        rasterization_state.pNext = nullptr;
        rasterization_state.flags = 0;
        rasterization_state.depthClampEnable = VK_FALSE;
        rasterization_state.rasterizerDiscardEnable = VK_FALSE;
        rasterization_state.polygonMode = VK_POLYGON_MODE_FILL;
        rasterization_state.cullMode = VK_CULL_MODE_NONE;
        rasterization_state.frontFace = VK_FRONT_FACE_CLOCKWISE;
        rasterization_state.depthBiasEnable = VK_FALSE;
        rasterization_state.depthBiasConstantFactor = 0.f;
        rasterization_state.depthBiasClamp = 0.f;
        rasterization_state.depthBiasSlopeFactor = 0.f;

        VkPipelineMultisampleStateCreateInfo multisample_state;
        multisample_state.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        multisample_state.pNext = nullptr;
        multisample_state.flags = 0;
        multisample_state.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
        multisample_state.sampleShadingEnable = VK_FALSE;
        multisample_state.minSampleShading = 1.f;
        multisample_state.pSampleMask = nullptr;
        multisample_state.alphaToCoverageEnable = VK_FALSE;
        multisample_state.alphaToOneEnable = VK_FALSE;

        VkPipelineColorBlendAttachmentState color_attachment_state;
        color_attachment_state.blendEnable = VK_FALSE;
        color_attachment_state.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
        color_attachment_state.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
        color_attachment_state.colorBlendOp = VK_BLEND_OP_ADD;
        color_attachment_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
        color_attachment_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
        color_attachment_state.alphaBlendOp = VK_BLEND_OP_ADD;
        color_attachment_state.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT;

        VkPipelineColorBlendStateCreateInfo color_state;
        color_state.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        color_state.pNext = nullptr;
        color_state.flags = 0;
        color_state.logicOpEnable = VK_FALSE;
        color_state.logicOp = VK_LOGIC_OP_COPY;
        color_state.attachmentCount = 1;
        color_state.pAttachments = &color_attachment_state;
        color_state.blendConstants[0] = 0.f;
        color_state.blendConstants[1] = 0.f;
        color_state.blendConstants[2] = 0.f;
        color_state.blendConstants[3] = 0.f;

        VkGraphicsPipelineCreateInfo info;
        info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        info.pNext = nullptr;
        info.flags = 0;
        info.stageCount = 2;
        info.pStages = shader_stages;
        info.pVertexInputState = &vertex_state;
        info.pInputAssemblyState = &input_state;
        info.pTessellationState = nullptr;
        info.pViewportState = &viewport_state;
        info.pRasterizationState = &rasterization_state;
        info.pMultisampleState = &multisample_state;
        info.pDepthStencilState = nullptr;
        info.pColorBlendState = &color_state;
        info.pDynamicState = nullptr;
        info.layout = layout;
        info.renderPass = render_pass;
        info.subpass = 0;
        info.basePipelineHandle = VK_NULL_HANDLE;
        info.basePipelineIndex = -1;

        VkResult res{ vkCreateGraphicsPipelines(backend->getDevice(), VK_NULL_HANDLE, 1, &info, nullptr, &pipeline) };
        if(res != VK_SUCCESS) {
            log->error("Failed to create graphics pipeline {}", res);
            throw std::runtime_error{ "Failed to create graphics pipeline" };
        }
    }

    void DisplayEngine::_createFramebuffers() {
        framebuffers.resize(swapchain_image_count);
        for(VkFramebuffer& framebuffer : framebuffers) {
            framebuffer = VK_NULL_HANDLE;
        }

        for(uint32_t i{ 0 }; i < swapchain_image_count; i++) {
            VkFramebufferCreateInfo info;
            info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = 0;
            info.renderPass = render_pass;
            info.attachmentCount = 1;
            info.pAttachments = &swapchain_views[i];
            info.width = display_size.width;
            info.height = display_size.height;
            info.layers = 1;

            VkResult res{ vkCreateFramebuffer(backend->getDevice(), &info, nullptr, &framebuffers[i]) };
            if(res != VK_SUCCESS) {
                log->error("Failed to create framebuffer {}", res);
                throw std::runtime_error{ "Failed to create framebuffer" };
            }
        }
    }

    void DisplayEngine::_createDescriptorSets() {
        VkDescriptorPoolSize size;
        size.descriptorCount = swapchain_image_count;
        size.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;

        VkDescriptorPoolCreateInfo info;
        info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        info.pNext = nullptr;
        info.flags = 0;
        info.maxSets = swapchain_image_count;
        info.poolSizeCount = 1;
        info.pPoolSizes = &size;

        VkResult res{ vkCreateDescriptorPool(backend->getDevice(), &info, nullptr, &descriptor_pool) };
        if(res != VK_SUCCESS) {
            log->error("Failed to create descriptor pool {}", res);
            throw std::runtime_error{ "Failed to create descriptor pool" };
        }

        std::vector<VkDescriptorSetLayout> set_layouts;
        set_layouts.resize(swapchain_image_count);
        for(VkDescriptorSetLayout& layout : set_layouts) {
            layout = set_layout;
        }

        VkDescriptorSetAllocateInfo alloc;
        alloc.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        alloc.pNext = nullptr;
        alloc.descriptorPool = descriptor_pool;
        alloc.descriptorSetCount = swapchain_image_count;
        alloc.pSetLayouts = set_layouts.data();

        descriptor_sets.resize(swapchain_image_count);
        res = vkAllocateDescriptorSets(backend->getDevice(), &alloc, descriptor_sets.data());
        if(res != VK_SUCCESS) {
            log->error("Failed to allocate descriptor sets {}", res);
            throw std::runtime_error{ "Failed to allocate descriptor sets" };
        }
    }

    void DisplayEngine::_updatePerImageSyncObjects() {
        uint32_t current_count{ static_cast<uint32_t>(draw_done_semaphores.size()) };
        
        if(swapchain_image_count == current_count) {
            // Nothing to do
        } else if(swapchain_image_count > current_count) {
            // Create new semaphores
            draw_done_semaphores.resize(swapchain_image_count);

            VkSemaphoreCreateInfo info;
            info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = 0;

            for(uint32_t i{ current_count }; i < swapchain_image_count; i++) {
                draw_done_semaphores[i] = VK_NULL_HANDLE;
            }

            for(uint32_t i{ current_count }; i < swapchain_image_count; i++) {
                VkResult res{ vkCreateSemaphore(backend->getDevice(), &info, nullptr, &draw_done_semaphores[i]) };
                if(res != VK_SUCCESS) {
                    log->error("Failed to create semaphore {}", res);
                    throw std::runtime_error{ "Failed to create semaphore" };
                }
            }
        } else {
            // Free old semaphores
            for(uint32_t i{ swapchain_image_count }; i < current_count; i++) {
                vkDestroySemaphore(backend->getDevice(), draw_done_semaphores[i], nullptr);
            }

            draw_done_semaphores.resize(swapchain_image_count);
        }
    }

    void DisplayEngine::_updateCommandBuffers() {
        uint32_t current_count{ static_cast<uint32_t>(cmd_buffers.size()) };

        if(swapchain_image_count == current_count) {
            // Nothing to do
        } else if(swapchain_image_count > current_count) {
            // Create new command buffers
            uint32_t diff{ swapchain_image_count - current_count };

            cmd_buffers.resize(swapchain_image_count);

            VkCommandBufferAllocateInfo info;
            info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
            info.pNext = nullptr;
            info.commandPool = cmd_pool;
            info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
            info.commandBufferCount = diff;

            VkResult res{ vkAllocateCommandBuffers(backend->getDevice(), &info, cmd_buffers.data() + current_count) };
            if(res != VK_SUCCESS) {
                // resize so that we dont destroy the failed allocation later
                cmd_buffers.resize(current_count);

                log->error("Failed to allocate command buffers {}", res);
                throw std::runtime_error{ "Failed to allocate command buffers" };
            }
        } else {
            // Free old command buffers
            uint32_t diff{ current_count - swapchain_image_count };

            vkFreeCommandBuffers(backend->getDevice(), cmd_pool, diff, cmd_buffers.data() + swapchain_image_count);

            cmd_buffers.resize(swapchain_image_count);
        }

        VkResult res{ vkResetCommandPool(backend->getDevice(), cmd_pool, 0) };
        if(res != VK_SUCCESS) {
            log->error("Failed to reset command pool {}", res);
            throw std::runtime_error{ "Failed to reset command pool" };
        }
    }

    void DisplayEngine::_prepareSwapchainRecreate() {
        VkResult res{ vkResetCommandPool(backend->getDevice(), cmd_pool, 0) };
        if(res != VK_SUCCESS) {
            log->error("Failed to reset command pool {}", res);
            throw std::runtime_error{ "Failed to reset command pool" };
        }

        vkDestroyDescriptorPool(backend->getDevice(), descriptor_pool, nullptr);
        descriptor_pool = VK_NULL_HANDLE;

        for(VkFramebuffer framebuffer : framebuffers) {
            vkDestroyFramebuffer(backend->getDevice(), framebuffer, nullptr);
        }
        framebuffers.clear();

        vkDestroyPipeline(backend->getDevice(), pipeline, nullptr);
        pipeline = VK_NULL_HANDLE;

        vkDestroyRenderPass(backend->getDevice(), render_pass, nullptr);
        render_pass = VK_NULL_HANDLE;

        for(VkImageView view : swapchain_views) {            
            vkDestroyImageView(backend->getDevice(), view, nullptr);
        }
        swapchain_views.clear();
        swapchain_images.clear();

        vkDestroySwapchainKHR(backend->getDevice(), swapchain, nullptr);
        swapchain = VK_NULL_HANDLE;
    }
}