#include "core_module.hpp"

namespace magma {

    CoreModule::~CoreModule() {
    }

    const std::string& CoreModule::getModuleName() const {
        return module_name;
    }

    Magma& CoreModule::getMagma() const {
        return engine;
    }

    CoreModule::CoreModule(Magma& engine, const char* module_name) : engine{ engine }, module_name{ module_name } {
        log = getOrCreateLogger(module_name);
        if(!log) {
            throw std::runtime_error{ "Failed to create logger for core module" };
        }
    }
}