#include "backend.hpp"

#include <limits>

namespace magma {

    VulkanBackend::~VulkanBackend() {
        _destroy();
    }

    void VulkanBackend::init(Logger logger) {
        {
            State expected{ State::UNINITIALIZED };
            if(!state.compare_exchange_strong(expected, State::INIT_STARTED)) {
                log->error("Tried to call VulkanBackend::init when instance is not in uninitialized state");
                throw std::runtime_error{ "Tried to call VulkanBackend::init when instance is not in uninitialized state" };
            }
        }

        try {
            if(!logger) {
                throw std::runtime_error{ "Tried to call VulkanBackend::init with invalid logger" };
            }

            log = logger;
            log->info("VulkanBackend initialization started");

            _initGLFW();
            _createInstance();
            _createDevice();
            _createAllocator();
            _createWindow();

        } catch(...) {
            state.store(State::FAILED);
            throw;
        }

        State expected{ State::INIT_STARTED };
        state.compare_exchange_strong(expected, State::INITIALIZED);
    }

    void VulkanBackend::destroy() {
        _destroy();
    }

    VkQueue VulkanBackend::getQueue(QueueId queue_id) {
        return queues.at(static_cast<size_t>(queue_id)).queue;
    }

    uint32_t VulkanBackend::getQueueFamily(QueueId queue_id) {
        return queues.at(static_cast<size_t>(queue_id)).family;
    }

    void VulkanBackend::_destroy() {
        State expected{ state.load() };
        if(expected != State::INITIALIZED && expected != State::FAILED) {
            return;
        }

        if(!state.compare_exchange_strong(expected, State::DESTROY_STARTED)) {
            return;
        }

        log->debug("Destroying VulkanBackend");

        if(device) {
            vkDeviceWaitIdle(device);
        }

        if(window) {
            glfwDestroyWindow(window);
            window = nullptr;
        }

        for(auto& queue : queues) {
            queue.queue = VK_NULL_HANDLE;
        }

        if(allocator) {
            vmaDestroyAllocator(allocator);
            allocator = nullptr;
        }

        if(device != VK_NULL_HANDLE) {
            vkDestroyDevice(device, nullptr);
            device = VK_NULL_HANDLE;
        }

        physical_device = VK_NULL_HANDLE;

        if(debug_messenger != VK_NULL_HANDLE) {
            debug_destroy(instance, debug_messenger, nullptr);
            debug_messenger = VK_NULL_HANDLE;
            debug_destroy = nullptr;
        }

        if(instance != VK_NULL_HANDLE) {
            vkDestroyInstance(instance, nullptr);
            instance = VK_NULL_HANDLE;
        }

        if(glfw_initialized) {
            glfwTerminate();
            glfw_initialized = false;
        }

        expected = State::DESTROY_STARTED;
        state.compare_exchange_strong(expected, State::UNINITIALIZED);
    }

    void VulkanBackend::_initGLFW() {
        glfw_log = getOrCreateLogger("GLFW");
        if(!glfw_log) {
            throw std::runtime_error{ "Failed to create GLFW logger" };
        }

        if(!glfwInit()) {
            log->error("Failed to initialize GLFW");
            throw std::runtime_error{ "Failed to initalized GLFW" };
        }
        glfw_initialized = true;
        
        glfwSetErrorCallback(VulkanBackend::_glfwErrorCb);
    }

    void VulkanBackend::_createInstance() {
        std::vector<const char*> extensions;
        std::vector<const char*> layers;

        _collectInstanceExtensions(extensions);
        _collectInstanceLayers(layers);

        VkApplicationInfo app_info;
        app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        app_info.pNext = nullptr;
        app_info.apiVersion = VK_API_VERSION_1_1;
        app_info.engineVersion = VK_MAKE_VERSION(0, 1, 0); // TODO
        app_info.pEngineName = "Magma";
        app_info.applicationVersion = VK_MAKE_VERSION(0, 1, 0); // TODO
        app_info.pApplicationName = "Magma";

        VkInstanceCreateInfo info;
        info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        info.pNext = nullptr;
        info.flags = 0;
        info.pApplicationInfo = &app_info;
        info.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
        info.ppEnabledExtensionNames = extensions.data();
        info.enabledLayerCount = static_cast<uint32_t>(layers.size());
        info.ppEnabledLayerNames = layers.data();

        VkResult res{ vkCreateInstance(&info, nullptr, &instance) };
        if(res != VK_SUCCESS) {
            log->error("Failed to create vulkan instance");
            throw std::runtime_error{ "Failed to create vulkan instance" }; // TODO
        }

        vk_log = getOrCreateLogger("Vulkan");
        if(!vk_log) {
            log->error("Failed to create vulkan logger");
            throw std::runtime_error{ "Failed to create vulkan logger" };
        }

        if(debug_vulkan) {
            PFN_vkCreateDebugUtilsMessengerEXT debug_create{ reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>(vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT")) };
            if(!debug_create) {
                log->error("Failed to find vkCreateDebugUtilsMessengerEXT function");
                throw std::runtime_error{ "Failed to find vkCreateDebugUtilsMessengerEXT function" };
            }

            debug_destroy = reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>(vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT"));
            if(!debug_destroy) {
                log->error("Failed to find vkDestroyDebugUtilsMessengerEXT function");
                throw std::runtime_error{ "Failed to find vkDestroyDebugUtilsMessengerEXT function" };
            }

            VkDebugUtilsMessengerCreateInfoEXT debug_info;
            debug_info.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
            debug_info.pNext = nullptr;
            debug_info.flags = 0;
            debug_info.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
            debug_info.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
            debug_info.pfnUserCallback = reinterpret_cast<PFN_vkDebugUtilsMessengerCallbackEXT>(_vulkanDebugCb);
            debug_info.pUserData = nullptr;

            res = debug_create(instance, &debug_info, nullptr, &debug_messenger);
            if(res != VK_SUCCESS) {
                log->error("Failed to create debug messenger");
                throw std::runtime_error{ "Failed to create debug messenger" };
            }
        }
    }

    void VulkanBackend::_createDevice() {
        std::vector<PhysicalDeviceInfo> device_infos;
        {
            std::vector<VkPhysicalDevice> devices;

            uint32_t size;
            vkEnumeratePhysicalDevices(instance, &size, nullptr);
            devices.resize(size);
            vkEnumeratePhysicalDevices(instance, &size, devices.data()); // TODO handle result

            device_infos.resize(size);
            for(uint32_t i{ 0 }; i < size; i++) {
                device_infos[i].device = devices[i];
            }
        }

        std::vector<const char*> extensions;
        _collectDeviceExtensions(extensions);
        std::vector<const char*> layers;
        _collectDeviceLayers(layers);

        size_t best_device{ std::numeric_limits<size_t>::max() };
        int best_rating{ 0 };
        for(size_t i{ 0 }; i < device_infos.size(); i++) {
            int rating{ _ratePhysicalDevice(device_infos[i], extensions, layers) };
            if(rating > best_rating) {
                best_rating = rating;
                best_device = i;
            }
        }

        if(best_device == std::numeric_limits<size_t>::max()) {
            log->error("Unable to find suitable physical device");
            throw std::runtime_error{ "Unable to find suitable physical device" };
        }

        PhysicalDeviceInfo& device_info{ device_infos[best_device] };
        physical_device = device_info.device;
        queues = device_info.queues;

        std::vector<VkDeviceQueueCreateInfo> queue_info;
        for(uint32_t family{ 0 }; family < device_info.queue_count.size(); family++) {
            uint32_t count{ device_info.queue_count[family] };
            if(count != 0) {
                queue_info.emplace_back();
                VkDeviceQueueCreateInfo& info{ queue_info.back() };

                float* queue_priorities = new float[count];
                for(uint32_t i{ 0 }; i < count; i++) {
                    queue_priorities[i] = 1.f;
                }

                info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
                info.pNext = nullptr;
                info.flags = 0;
                info.queueFamilyIndex = family;
                info.queueCount = count;
                info.pQueuePriorities = queue_priorities;
            }
        }

        VkDeviceCreateInfo info;
        info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        info.pNext = nullptr;
        info.flags = 0;
        info.queueCreateInfoCount = static_cast<uint32_t>(queue_info.size());
        info.pQueueCreateInfos = queue_info.data();
        info.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
        info.ppEnabledExtensionNames = extensions.data();
        info.enabledLayerCount = static_cast<uint32_t>(layers.size());
        info.ppEnabledLayerNames = layers.data();
        info.pEnabledFeatures = &device_info.features;

        VkResult res{ vkCreateDevice(device_info.device, &info, nullptr, &device) };

        for(VkDeviceQueueCreateInfo& info : queue_info) {
            delete[] info.pQueuePriorities;
        }

        if(res != VK_SUCCESS) {
            log->error("Failed to create logical device {}", res);
            throw std::runtime_error{ "Failed to create logical device" };
        }

        for(auto& queue : queues) {
            vkGetDeviceQueue(device, queue.family, queue.index, &queue.queue);
        }
    }

    void VulkanBackend::_createAllocator() {
        VmaAllocatorCreateInfo info{};
        info.flags = VMA_ALLOCATOR_CREATE_EXT_MEMORY_BUDGET_BIT;
        info.physicalDevice = physical_device;
        info.device = device;
        info.preferredLargeHeapBlockSize = 0;
        info.pAllocationCallbacks = nullptr;
        info.pDeviceMemoryCallbacks = nullptr;
        info.frameInUseCount = 2;
        info.pHeapSizeLimit = nullptr;
        info.pVulkanFunctions = nullptr;
        info.pRecordSettings = nullptr;
        info.instance = instance;
        info.vulkanApiVersion = VK_API_VERSION_1_1;

        VkResult res{ vmaCreateAllocator(&info, &allocator) };
        if(res != VK_SUCCESS) {
            log->error("Failed to create vma allocator {}", res);
            throw std::runtime_error{ "Failed to create vma allocator" };
        }
    }

    void VulkanBackend::_createWindow() {
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        window = glfwCreateWindow(800, 600, "Magma", nullptr, nullptr);
        if(!window) {
            log->error("Failed to create window");
            throw std::runtime_error{ "Failed to create window" };
        }
    }

    int VulkanBackend::_ratePhysicalDevice(
            PhysicalDeviceInfo& device,
            const std::vector<const char*>& required_extensions, 
            const std::vector<const char*>& required_layers) {

        vkGetPhysicalDeviceProperties(device.device, &device.properties);

        vkGetPhysicalDeviceFeatures(device.device, &device.features);

        std::vector<VkQueueFamilyProperties> queue_families;
        uint32_t size;
        vkGetPhysicalDeviceQueueFamilyProperties(device.device, &size, nullptr);
        queue_families.resize(size);
        vkGetPhysicalDeviceQueueFamilyProperties(device.device, &size, queue_families.data());

        device.queue_count.resize(size);

        bool graphics_found{ false }, transfer_found{ false }, compute_found{ false }, display_found{ false };
        for(uint32_t i{ 0 }; i < size; i++) {
            const VkQueueFamilyProperties& queue{ queue_families[i] };
            uint32_t queue_count{ 0 };

            if(!graphics_found) {
                if(queue.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
                    graphics_found = true;
                    QueueInfo &queue{ device.queues[static_cast<size_t>(QueueId::RENDER)] };
                    queue.family = i;
                    queue.index = queue_count++;
                }
            }

            if(!transfer_found) {
                if(queue.queueFlags & VK_QUEUE_TRANSFER_BIT) {
                    transfer_found = true;
                    QueueInfo &queue{ device.queues[static_cast<size_t>(QueueId::TRANSFER)] };
                    queue.family = i;
                    queue.index = queue_count++;
                }
            }

            if(!compute_found) {
                if(queue.queueFlags & VK_QUEUE_COMPUTE_BIT) {
                    compute_found = true;
                    QueueInfo &queue{ device.queues[static_cast<size_t>(QueueId::COMPUTE)] };
                    queue.family = i;
                    queue.index = queue_count++;
                }
            }

            if(!display_found) {
                if((glfwGetPhysicalDevicePresentationSupport(instance, device.device, i) == GLFW_TRUE) && (queue.queueFlags & VK_QUEUE_GRAPHICS_BIT)) {
                    display_found = true;
                    QueueInfo &queue{ device.queues[static_cast<size_t>(QueueId::DISPLAY)] };
                    queue.family = i;
                    queue.index = queue_count++;
                }
            }

            device.queue_count[i] = queue_count;

            if(queue_count > queue.queueCount) {
                return -1;
            }
        }

        std::vector<VkExtensionProperties> ext_props;

        vkEnumerateDeviceExtensionProperties(device.device, nullptr, &size, nullptr);
        ext_props.resize(size);
        vkEnumerateDeviceExtensionProperties(device.device, nullptr, &size, ext_props.data());

        uint32_t matches{ 0 };
        for(VkExtensionProperties& ext : ext_props) {
            for(const char* name : required_extensions) {
                if(strcmp(name, ext.extensionName) == 0) {
                    matches++;
                }
            }
        }
        if(matches != required_extensions.size()) {
            return -1;
        }

        int rating{ 1 };
        switch(device.properties.deviceType) {
            case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:
                rating *= 100;
                break;
            case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:
                rating *= 10;
                break;
            default:
                rating *= 1;
                break;
        }

        return rating;
    }

    void VulkanBackend::_collectInstanceExtensions(std::vector<const char*>& extensions) {
        if(debug_vulkan) {
            extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
            extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
        }

        uint32_t glfw_count;
        const char** glfw_exts{ glfwGetRequiredInstanceExtensions(&glfw_count) };

        for(uint32_t i{ 0 }; i < glfw_count; i++) {
            bool found{ false };

            for(const char* ext : extensions) {
                if(strcmp(ext, glfw_exts[i]) == 0) {
                    found = true;
                    break;
                }
            }

            if(!found) {
                extensions.push_back(glfw_exts[i]);
            }
        }
    }

    void VulkanBackend::_collectInstanceLayers(std::vector<const char*>& layers) {
        if(debug_vulkan) {
            layers.push_back("VK_LAYER_LUNARG_standard_validation");
        }
    }

    void VulkanBackend::_collectDeviceExtensions(std::vector<const char*>& extensions) {
        extensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
    }

    void VulkanBackend::_collectDeviceLayers(std::vector<const char*>& layers) {
    }

    Logger VulkanBackend::vk_log;
    VKAPI_ATTR VkBool32 VKAPI_CALL VulkanBackend::_vulkanDebugCb(
            VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
            VkDebugUtilsMessageTypeFlagsEXT messageType,
            const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
            void* pUserData) {
        
        if(messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) {
            vk_log->warn("{0}", pCallbackData->pMessage);
        }
        else if(messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT) {
            vk_log->error("{0}", pCallbackData->pMessage);
        } else {
            vk_log->info("{0}", pCallbackData->pMessage);
        }

        return VK_TRUE;
    }

    Logger VulkanBackend::glfw_log;
    void VulkanBackend::_glfwErrorCb(int code, const char* msg) {
        glfw_log->error(msg);
    }
}