#version 450

layout(location = 0) in vec2 uv_in;
layout(binding = 0) uniform sampler2D source;

layout(location = 0) out vec4 color;

void main() {
    color = texture(source, uv_in);
    //color = vec4(uv_in, 0.0, 1.0);
}